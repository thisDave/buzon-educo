<?php 
// Como primer paso ejecuto las constantes creadas en el archivo de configuración

require_once 'config/configuracion.php';
require_once 'controladores/controlador.php';

// Evaluo si la sesión está llena o vacía
if (!empty($_SESSION))
{
	//Determino si la sesión sesion está definida
	if (isset($_SESSION['sesion']))
	{
		//Evaluo el valor de sesion y muestro las vistas
		switch ($_SESSION['sesion']) 
		{
			case 'administrador':

				require_once 'app/controladores/controlador-admin.php';

				if (isset($_SESSION['gestion']))
				{
					switch ($_SESSION['gestion'])
					{
						case 'nuevo-caso':
							require_once 'vistas/administradores/nuevo-caso.php';
						break;

						case 'casos-pendientes':
							require_once 'vistas/administradores/casos-pendientes.php';
						break;

						case 'casos-abiertos':
							require_once 'vistas/administradores/casos-abiertos.php';
						break;

						case 'casos-cerrados':
							require_once 'vistas/administradores/casos-cerrados.php';
						break;

						case 'casos-antiguos':
							require_once 'vistas/administradores/casos-antiguos.php';
						break;

						case 'casos-charts':
							require_once 'vistas/administradores/casos-charts.php';
						break;

						case 'perfil-pendiente':
							require_once 'vistas/administradores/perfil-pendiente.php';
						break;

						case 'perfil-abierto':
							require_once 'vistas/administradores/perfil-abierto.php';
						break;

						case 'perfil-cerrado':
							require_once 'vistas/administradores/perfil-cerrado.php';
						break;

						/*---------------------------------------------------------*/

						case 'comite-integrantes':
							require_once 'vistas/administradores/comite-integrantes.php';
						break;

						case 'comite-propuestas':
							require_once 'vistas/administradores/comite-propuestas.php';
						break;

						case 'comite-propuestas-perfil':
							require_once 'vistas/administradores/comite-propuestas-perfil.php';
						break;

						case 'documentos':
							require_once 'vistas/administradores/documentos.php';
						break;

						case 'mensajes':
							require_once 'vistas/administradores/mensajes.php';
						break;

						case 'cuenta':
							require_once 'vistas/administradores/cuenta.php';
						break;

						case 'ayuda':
							require_once 'vistas/administradores/ayuda.php';
						break;

						/*---------------------------------------------------------*/

						case 'reportes-pdf':
							require_once 'vistas/administradores/reportes-pdf.php';
						break;

						case 'reportes-xls':
							require_once 'vistas/administradores/reportes-xls.php';
						break;
					}
				}
				else
				{
					require_once 'vistas/administradores/inicio.php';
				}

			break;
		}
	}
	else if (isset($_SESSION['buzon']))
	{
		switch ($_SESSION['buzon'])
		{
			case 'inicio': require_once 'vistas/inicio/inicio.php'; break;

			case 'version-completa': require_once 'vistas/inicio/version-completa.php'; break;

			case 'version-anonima': require_once 'vistas/inicio/version-anonima.php'; break;

			case 'login': require_once 'vistas/inicio/login.php'; break;

			case 'reset': require_once 'vistas/inicio/reset.php'; break;
		}
	}
}
else
{
	require_once 'vistas/inicio/inicio.php';
}

?>