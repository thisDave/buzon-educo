<?php

	/* Llamamos el modelo e instanciamos su clase */

	require_once APP.'/modelos/admin.php';
	
	$obj = new Admin;

	if ($objModelo->getUrl())
	{
		$url = $objModelo->getUrl();
		switch ($url['url'])
		{
			case 'nuevo-caso':
				$_SESSION['gestion'] = 'nuevo-caso';
			break;

			/* Inicio DropDown Casos */

			case 'casos-pendientes':
				$_SESSION['gestion'] = 'casos-pendientes';
			break;

			case 'casos-abiertos':
				$_SESSION['gestion'] = 'casos-abiertos';
			break;

			case 'casos-cerrados':
				$_SESSION['gestion'] = 'casos-cerrados';
			break;

			case 'casos-antiguos':
				$_SESSION['gestion'] = 'casos-antiguos';
			break;

			case 'casos-charts':
				$_SESSION['gestion'] = 'casos-charts';
				$_SESSION['chart'] = $url['codigo'];
			break;

			case 'perfil-pendiente':
				$_SESSION['gestion'] = 'perfil-pendiente';
				$_SESSION['codigoCaso'] = $url['codigo'];
			break;

			case 'perfil-abierto':
				$_SESSION['gestion'] = 'perfil-abierto';
				$_SESSION['codigoCaso'] = $url['codigo'];
			break;

			case 'perfil-cerrado':
				$_SESSION['gestion'] = 'perfil-cerrado';
				$_SESSION['codigoCaso'] = $url['codigo'];
			break;

			case 'eliminar-entrada':
				$codigoRegistro = substr($_SESSION['codigoCaso'], 4);
				$fecha = date('Y-m-d');
				$query = "CALL SP_eliminarEntrada({$url['codigo']}, {$codigoRegistro}, {$_SESSION['codUsuario']}, '{$fecha}')";

				if (!$objModelo->execute_query($query))
				{
					$_SESSION['error'] = "on";
				}
			break;

			/* Fin DropDown Casos */

			//-------------------------------------------------------------------

			/* Inicio DropDown Comité */

			case 'comite-integrantes':
				$_SESSION['gestion'] = 'comite-integrantes';
			break;

			case 'comite-propuestas':
				$_SESSION['gestion'] = 'comite-propuestas';
			break;

			case 'comite-propuestas-perfil':
				$_SESSION['gestion'] = 'comite-propuestas-perfil';
				$_SESSION['codigoPropuesta'] = $url['codigo'];
			break;

			/* Fin DropDown Comité */

			//-------------------------------------------------------------------

			/* Inicio Documentos */

			case 'documentos':
				$_SESSION['gestion'] = 'documentos';
			break;

			/* Fin Documentos */

			//-------------------------------------------------------------------

			/* Inicio Documentos */

			case 'mensajes':
				$_SESSION['gestion'] = 'mensajes';
			break;

			case 'newMnsj':
				$_SESSION['mensaje'] = true;
			break;

			case 'deleteMnsj':
				unset($_SESSION['mensaje']);
			break;

			/* Fin Mensajes */

			//-------------------------------------------------------------------

			/* Inicio Cuenta */

			case 'cuenta':
				$_SESSION['gestion'] = 'cuenta';
			break;

			case 'editar-persona':
				$_SESSION['editar-persona']	= true;
			break;

			case 'bloquear-persona':
				unset($_SESSION['editar-persona']);
			break;

			case 'editar-usuario':
				$_SESSION['editar-usuario']	= true;
			break;

			case 'bloquear-usuario':
				unset($_SESSION['editar-usuario']);
			break;

			case 'verificarUsuario':
				$_SESSION['valor_txt'] = $url['codigo'];
			break;

			/* Fin Cuenta */

			//-------------------------------------------------------------------
			
			/* Inicio ayuda */

			case 'ayuda':
				$_SESSION['gestion'] = 'ayuda';
			break;

			/* Fin ayuda */

			//-------------------------------------------------------------------

			/* Inicio de Acciones varias */

			case 'reportes-pdf':
				$_SESSION['gestion'] = 'reportes-pdf';
				$_SESSION['caso'] = $url['codigo'];
			break;

			case 'reportes-xls':
				$_SESSION['gestion'] = 'reportes-xls';
				$_SESSION['caso'] = $url['codigo'];
			break;

			case 'educo':
				unset($_SESSION['gestion']);
			break;

			/* Fin de acciones varias */
		}

		header("Location: ".URL);
	}

	/* CASOS */

	if (isset($_POST['buscar-caso']))
	{
		if (empty($_POST['clave']))
		{
			if (isset($_SESSION['clave-busqueda']))
			{
				unset($_SESSION['clave-busqueda']);
			}
		}
		else
		{
			$_SESSION['clave-busqueda'] = $_POST['clave'];
		}

		header("Location: ".URL);
	}

	if (isset($_POST['abrir-caso']))
	{
		$fecha = date('Y-m-d');
		$query = "CALL SP_abrirCaso({$_POST['abrir-caso']}, {$_SESSION['codUsuario']}, '{$fecha}')";

		if ($objModelo->execute_query($query))
		{
			header("Location:".URL."perfil-abierto/".$_SESSION['codigoCaso']);
		}
		else
		{
			$_SESSION['error'] = "on";
			header("Location:".URL);
		}
	}

	if (isset($_POST['ingresar-entrada']))
	{
		$titulo = $_POST['titulo'];
		$descripcion = $_POST['descripcion'];
		$entrada = $_POST['ingresar-entrada'];
		$codUser = $_SESSION['codUsuario'];
		$fecha = date('Y-m-d');

		$query = "CALL SP_ingresarEntrada('{$titulo}', '{$descripcion}', {$entrada}, {$codUser}, '{$fecha}')";

		if (!$objModelo->execute_query($query))
		{
			$_SESSION['error'] = "on";
		}

		header("Location: ".URL);
	}

	if (isset($_POST['subir-doc']))
	{
		try 
		{
			if (isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > 120000000)
			{
				$size = round($_SERVER['CONTENT_LENGTH'] / 1000000);
				throw new Exception('[ Tamaño no permitido, el tamaño <strong>máximo</strong> es de 100MB y el documento que trata de subir es de: '.$size.'Mb ]');
			}
			else
			{
				$codEntrada = $_POST['subir-doc'];
				$codRegistro = $_POST['codRegistro'];
				$archivo = $_FILES['archivo']['name'];
				$nombre = $_POST['nombre'];

				unset($_SESSION['vars']);

				if (!$obj->subirArchivo($codRegistro, $codEntrada, $archivo, $nombre))
				{
					$_SESSION['error'] = "on";
				}
				
				header("Location: ".URL);
			}

		}
		catch (Exception $e) 
		{
			$_SESSION['error'] = "on";
			$_SESSION['mnsj'] = $e->getMessage();
			header("Location: ".URL);
		}
	}

	if (isset($_POST['docs-subir-doc']))
	{
		try 
		{
			if (isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > 120000000)
			{
				$size = round($_SERVER['CONTENT_LENGTH'] / 1000000);
				throw new Exception('[ Tamaño no permitido, el tamaño <strong>máximo</strong> es de 100MB y el documento que trata de subir es de: '.$size.'Mb ]');
			}
			else
			{
				$archivo = $_FILES['file']['name'];
				$nombre = $_POST['nombreDoc'];

				if ($obj->subirDoc($nombre, $archivo))
				{
					setcookie("success","El documento ha sido cargado exitosamente!");
				}
				else
				{
					setcookie("fail","El documento no pudo ser enviado, por favor intentelo nuevamente.");
				}
				
				header("Location: ".URL);
			}

		}
		catch (Exception $e) 
		{
			$_SESSION['error'] = "on";
			$_SESSION['mnsj'] = $e->getMessage();
			header("Location: ".URL);
		}
	}

	if (isset($_POST['descargar']))
	{
		$obj->descargar_archivo($_POST['descargar'], 1);
	}

	if (isset($_POST['descargarDoc']))
	{
		$obj->descargar_archivo($_POST['descargarDoc'], 2);
	}

	if (isset($_POST['editar-entrada']))
	{
		$query = "CALL SP_editarEntrada('".$_POST['titulo']."', '".$_POST['descripcion']."', ".$_POST['editar-entrada'].", ".$_SESSION['codUsuario'].")";

		if (!$objModelo->execute_query($query))
		{
			$_SESSION['error'] = "on";
		}

		header("Location: ".URL);
	}

	if (isset($_POST['eliminar-documento']))
	{
		$query = "CALL SP_eliminarDocumento(".$_POST['eliminar-documento'].", ".$_SESSION['codUsuario'].")";

		if (!$objModelo->execute_query($query))
		{
			$_SESSION['error'] = "on";
		}

		header("Location: ".URL);
	}

	if (isset($_POST['cerrar-caso']))
	{
		$codigo = substr($_SESSION['codigoCaso'], 4);
		$fecha = date('Y-m-d');
		$query = "CALL SP_cerrarCaso({$codigo}, {$_SESSION['codUsuario']}, '{$fecha}')";

		if (!$objModelo->execute_query($query))
		{
			$_SESSION['error'] = "on";
		}
		else
		{
			$_SESSION['gestion'] = 'perfil-cerrado';
		}

		header("Location: ".URL);
	}

	/* COMITE */

	if (isset($_POST['guardar-propuesta']))
	{
		$fecha = date('Y-m-d');
		$query = "CALL SP_nueva_propuesta('{$_POST['titulo-propuesta']}', '{$_POST['descripcion-propuesta']}', {$_SESSION['codUsuario']}, '{$fecha}')";

		if($objModelo->execute_query($query))
		{
			header("Location:".URL."comite-propuestas");
		}
		else
		{
			header("Location:".URL);
		}
	}

	/* CUENTA */

	if (isset($_POST['saveUser']))
	{
		if ($_POST['saveUser'] == 'persona')
		{
			$obj->update_user('person', $_POST['nombres'], $_POST['apellidos'], $_POST['cargo'], $_POST['depto'], $_POST['email'], $_POST['office'], $_POST['pais'],'','','');
		}
		else
		{
			$obj->update_user('user','','','','','','','', $_POST['user'], $_POST['pass'], $_POST['pass2']);
		}

		header("Location: ".URL);
	}

	if (isset($_POST['saveNewPass']))
	{
		$obj->updatePass($_POST['pass'], $_POST['pass2']);

		header("Location: ".URL);
	}
?>