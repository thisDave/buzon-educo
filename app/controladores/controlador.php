<?php

	/* Llamamos el modelo e instanciamos su clase */
	require_once APP.'/modelos/modelo.php';

	$objModelo = new Modelo;

	if ($objModelo->getUrl())
	{
		$url = $objModelo->getUrl();

		switch ($url['url'])
		{
			case 'login':
				$_SESSION['buzon'] = 'login';
			break;

			case 'version_completa':
				$_SESSION['buzon'] = 'version-completa';
			break;

			case 'version_anonima':
				$_SESSION['buzon'] = 'version-anonima';
			break;

			case 'inicio':
				$_SESSION['buzon'] = 'inicio';
			break;

			case 'reset':
				$objModelo->validaReset($url['codigo']);
			break;

			case 'cancelReset':
				$objModelo->cancelReset();
				$_SESSION['buzon'] = 'login';
			break;

			header("Location: ".URL);
		}
	}

	if (isset($_POST['login']))
	{
		if ($objModelo->login($_POST['usuario'], $_POST['password']))
		{
			if (isset($_SESSION['error_login'])) { unset($_SESSION['error_login']); }
			unset($_SESSION['buzon']);
		}
		
		header("Location: ".URL);
	}

	if (isset($_POST['logout']))
	{
		session_destroy();
		header("Location: ".URL);
	}

	/***** Insertando Registros *****/

	if (isset($_POST['registroCompleto']))
	{
		$email = $_POST['correo']."@educo.org";

		$objModelo->registrarCaso(1, $_POST['nombre'], $email, $_POST['ol'], $_POST['tipo'], $_POST['asunto'], $_POST['mensaje']);

		header("Location: ".URL);
	}

	if (isset($_POST['registroAnonimo']))
	{
		$objModelo->registrarCaso(2, 'N/A', 'N/A', $_POST['ol'], $_POST['tipo'], $_POST['asunto'], $_POST['mensaje']);

		header("Location: ".URL.'version_anonima');
	}

	if (isset($_POST['registroCasoLogin']))
	{
		$usuario = $objModelo->info_usuario($_SESSION['codUsuario']);
		$nombre = $usuario['nombres'][0].' '.$usuario['apellidos'][0];

		$objModelo->registrarCaso(1, $nombre, $usuario['email'][0], $_POST['ol'], $_POST['tipo'], $_POST['asunto'], $_POST['mensaje']);

		header("Location: ".URL.'nuevo-caso');
	}

	if (isset($_POST['btnForget']))
	{
		if ($_POST['email'] != '')
		{
			if ($objModelo->validaMail($_POST['email']))
			{
				$_SESSION['email'] = $_POST['email'];
				setcookie('forget', 'Estamos validando y enviando la información a tu correo institucional.');
			}
			else
			{
				setcookie('fail', 'Para restablecer tu contraseña, debes ingresar tu correo <strong>institucional</strong><br><br>Ejemplo: <strong>nombre.apellido@educo.org</strong>');
			}
		}
		else
		{
			setcookie('fail', 'Debes llenar el campo de correo electrónico para restablecer tu contraseña.');
		}

		header('Location: '.URL.'login');
	}

	if (isset($_POST['resetPass']))
	{
		$objModelo->updatePass($_POST['pass'], $_POST['pass2'], $_POST['resetPass']);

		session_destroy();

		header("Location: ".URL.'login');
	}
	
?>