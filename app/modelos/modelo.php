<?php 

	require_once APP.'/config/conexion.php';
	require_once APP.'/tools/phpMailer/PHPMailerAutoload.php';

	class Modelo extends Conexion
	{
		public function getUrl()
		{
			if (isset($_GET['url']))
			{
				$pos = strpos($_GET['url'], '/');

				if ($pos) 
				{
					$url = rtrim($_GET['url'], '/');
					$url = filter_var($url, FILTER_SANITIZE_URL);
					$url = explode('/', $url);

					$len = strlen($url[0]) + 1;

					$codigo = substr($_GET['url'], $len);

					$url = [
						'url' => $url[0],
						'codigo' => $codigo
					];
				}
				else
				{
					$url = [
						'url' => $_GET['url']
					];
				}

				return $url;
			}
			else
				return false;
		}

		public function execute_query($query)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$resultado = $conexion->query($query);

			if ($resultado)
				return true;
			else
				return false;

			parent::desconectar();
		}

	    public function login($user, $pass)
		{
			$conexion = parent::conectar();

			$resultado = $conexion->query("CALL SP_login('".$user."', '".$pass."')");

			if ($resultado->num_rows > 0)
			{
				$datos = $resultado->fetch_array(MYSQLI_ASSOC);
				
				if ($datos['estado'] == 'Activo')
				{
					$_SESSION['codUsuario'] = $datos['codUsuario'];
				
					switch ($datos['permiso'])
					{
						case 'administrador': $_SESSION['sesion'] = 'administrador'; break;

						case 'usuarios': $_SESSION['sesion'] = 'administrador'; break;

						case 'integrante': $_SESSION['sesion'] = 'administrador'; break;
					}

					return true;
				}
				else
				{
					$_SESSION['error_login'] = "El usuario ingresado se encuentra inactivo";
					return false;
				}
			}
			else
			{
				$_SESSION['error_login'] = "Usuario y/o contraseña incorrectos";
				return false;
			}

			mysqli_free_result($resultado);

			parent::desconectar();
		}

		public function registrarCaso($tipo, $nombre, $email, $ol, $caso, $asunto, $mensaje)
		{
			$nombre = $this->comillaSimple($nombre);
			$email = $this->comillaSimple($email);
			$asunto = $this->comillaSimple($asunto);
			$mensaje = $this->comillaSimple($mensaje);
			$fecha = date('Y-m-d');

			$query = "CALL SP_registrarCaso({$tipo}, '{$nombre}', '{$email}', {$ol}, {$caso}, '{$asunto}', '{$mensaje}', '{$fecha}')";

			$_SESSION['query'] = $query;

			if($this->execute_query($query))
				setcookie("success","El mensaje ha sido enviado exitosamente!");
			else
				setcookie("fail","No se pudo enviar el mensaje, por favor intentalo nuevamente, de persistir el error contactate con el administrador del sitio.");
		}

		public function comillaSimple($string)
		{
			$pos = strpos($string, '\'');

			if ($pos)
			{
				$array = str_split($string);
				$newString = '';
				foreach ($array as $key => $value)
				{
					if ($key == $pos)
						$newString .= '\\'.$value;
					else
						$newString .= $value;
				}

				return $newString;
			}
			else{
				return $string;
			}
		}

		public function info_usuario($codUsuario)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$resultado = $conexion->query("CALL SP_infoUsuario(".$codUsuario.")");

			if ($resultado->num_rows > 0)
			{
				$usuario = [];

				while ($fila = $resultado->fetch_assoc())
				{
					$usuario['codUsuario'][] = $fila['codUsuario'];
					$usuario['nombres'][] = $fila['nombres'];
					$usuario['apellidos'][] = $fila['apellidos'];
					$usuario['cargo'][] = $fila['cargo'];
					$usuario['depto'][] = $fila['depto'];
					$usuario['usuario'][] = $fila['usuario'];
					$usuario['email'][] = $fila['email'];
					$usuario['oficina'][] = $fila['oficina'];
					$usuario['permiso'][] = $fila['permiso'];
					$usuario['estado'][] = $fila['estado'];
					$usuario['codPais'][] = $fila['codPais'];
					$usuario['pais'][] = $fila['pais'];
				}

				return $usuario;	
			}
			else
			{
				return false;
			}

			$resultado->free();

			parent::desconectar();
		}

		public function validaMail($email)
		{
			$pos = strpos($email, '@');

			$dominio = substr($email, $pos);

			if ($dominio == '@educo.org')
				return true;
			else
				return false;
		}

		public function validaReset($url)
		{
			$conexion = parent::conectar();

			$codigo = substr($url, 12, -12);

			$nCodigo = strlen($codigo);

			$claveDesbloqueada1 = substr($url, 0, -11 - ($nCodigo+2));

			$claveDesbloqueada2 = substr($url, -11);
			
			$query = "
			SELECT 
				COUNT(*) AS 'total' 
			FROM 
				tbl_usuarios 
			WHERE 
				codUsuario = ".$codigo." 
				AND codReset = 1 
				AND primeraClave = '".$claveDesbloqueada1."' 
				AND segundaclave = '".$claveDesbloqueada2."'";
			
			$resultado = $conexion->query($query);
			
			if ($resultado)
			{
				while ($fila = $resultado->fetch_assoc())
				{
				    $total = $fila['total'];
				}

				if ($total > 0)
				{
					$_SESSION['buzon'] = 'reset';
					$_SESSION['codUsuario'] = $codigo;
				}
				else
				{
					$_SESSION['buzon'] = 'login';
				}
			}
			else
			{
				$_SESSION['buzon'] = 'login';
			}

			parent::desconectar();

			header("Location: ".URL);
		}

		public function updatePass($pass, $pass2, $codUser)
		{
			if ($pass == $pass2)
			{
				$passCrypt = md5(sha1(md5($pass)));

				$query = "
				UPDATE 
					tbl_usuarios 
				SET 
					pass = '".$passCrypt."', 
					codReset = 0, 
					primeraClave = 'empty', 
					segundaclave = 'empty' 
				WHERE 
					codUsuario = ".$codUser;

				if ($this->execute_query($query))
				{
					setcookie('success', 'La contraseña ha sido actualizada exitosamente.');
				}
				else
				{
					setcookie('fail', 'Surgió un problema al enviar los datos, intentalo más tarde.');
				}
			}
			else
			{
				setcookie('fail', 'Las contraseñas no coinciden');
			}
		}

		public function cancelReset()
		{
			$query = "UPDATE tbl_usuarios SET codReset = 0 WHERE codUsuario = ".$_SESSION['codUsuario'];

			if ($this->execute_query($query))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function restore_pass()
		{
			$conexion = parent::conectar();
			
			$query = "SELECT * FROM tbl_usuarios WHERE email = '".$_SESSION['email']."'";

			$resultado = $conexion->query($query);

			if ($resultado->num_rows > 0)
			{
				while ($row = $resultado->fetch_assoc())
				{
					$codUsuario = $row['codUsuario'];
					$usuario = $row['nombres'].' '.$row['apellidos'];
					$email = $row['email'];
				}

				$clave1 = $this->generaClave();
				$clave2 = $this->generaClave();

				$url = $clave1.'('.$codUsuario.')'.$clave2;

				$consulta = "UPDATE tbl_usuarios SET codReset = 1, primeraClave = '".$clave1."', segundaClave = '".$clave2."' WHERE codUsuario = ".$codUsuario;

				if ($this->execute_query($consulta))
				{
					$asunto = 'Restablecer de contraseña';

					$html = '
					<!DOCTYPE html>
					<html lang="es-SV">
						<head>
							<!-- Required meta tags -->
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
							<title>Buzón - Educo</title>
							<!-- Bootstrap CSS -->
							<link rel="stylesheet" href="css/bootstrap.css">
						</head>
						<body>
							<div class="container-fluid">
								<div class="row mt-3">
									<div class="col-3"></div>
									<div class="col-6 border border-dark">
										<div class="container">
											<div class="row mt-2">
												<div class="col-12 text-center">
													<img src="img/logo.png" class="rounded" width="150" height="120">
												</div>
											</div>
											<div class="row mt-2">
												<div class="col-12 text-center">
													<h3 class="display-4">Restablecer contraseña</h3>
												</div>
											</div>
											<div class="row mt-3">
												<div class="col-12 text-center">
													<h4>Hola '.$usuario.'</h4>
												</div>
											</div>
											<div class="row">
												<div class="col-12 text-center">
													<p>Recibimos una solicitud para restablecer tu contraseña.<br> Si fuiste tú, haz clic sobre el siguiente link:</p>
												</div>
											</div>
											<div class="row">
												<div class="col-12 text-center">
													<a href="'.URL.'reset/'.$url.'" class="btn btn-primary" target="_blank">RESTABLECER CONTRASEÑA</a>
												</div>
											</div>
											<div class="row mt-3">
												<div class="col-12 text-center">
													<p>
														Si no quieres restablecer tu contraseña, ignora este mensaje y continua ingresando con tu contraseña actual.
													</p>
													<p>
														Gracias por confiar en nosotros.
													</p>
													<p>
														Atentamente:<br>
														<strong>Sistemas Educo El Salvador</strong>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-3"></div>
								</div>
							</div>		
						</body>
					</html>
					';

					if ($this->sendMail($usuario, $email, $asunto, $html))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					setcookie('fail', 'Hubo un problema al actualizar la contraseña en la base de datos, por favor contacte al administrador del sistema para solventar el problema.');
				}
			}
			else
			{
				setcookie('fail', 'Por favor ingresa un correo válido.');
			}

			parent::desconectar();
		}

		private function sendMail($nombre, $email, $asunto, $html)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');
			
			$mensaje = $html;
			$mail = new PHPMailer;
			//indico a la clase que use SMTP
			$mail->IsSMTP();
			//permite modo debug para ver mensajes de las cosas que van ocurriendo
			//$mail->SMTPDebug = 2;
			//Debo de hacer autenticación SMTP
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "tls"; //ssl
			//indico el servidor de Gmail para SMTP
			$mail->Host = "smtp.office365.com"; //smtp.gmail.com
			//indico el puerto que usa Gmail
			$mail->Port = 587; //465 - gmail
			//indico un usuario / clave de un usuario de gmail
			$mail->Username = "educo@educo.sv";
			$mail->Password = "$3duco2018";
			$mail->CharSet = "UTF-8";
			$mail->From = "educo@educo.sv";
			$mail->FromName = "EducoSV";
			$mail->Subject = $asunto;
			$mail->addAddress($email, $nombre);
			$mail->MsgHTML($mensaje);

			if($mail->Send()){
				return true;
			}else{
				return false;
			}
			parent::desconectar();	
		}

		private function generaClave()
		{
		    //Creamos la contraseña        
		    $cadena = "ABCDFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		    $longitudCadena = strlen($cadena);
		    $pass = "";
		    $longitudPass = 11;
		    for($i=1 ; $i<=$longitudPass ; $i++){
		        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
		        $pos=rand(0,$longitudCadena-1);
		     
		        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
		        $pass .= substr($cadena,$pos,1);
		    }
		    return $pass;
		}
	}
?>
