<?php

	/**
	 * Creamos la clase que gestionará toda la app llamando la conexion
	 */

	require_once APP.'/config/conexion.php';

	class Admin extends Conexion
	{
		/* Metodos de administración */

		public function execute_query($query)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$resultado = $conexion->query($query);

			if ($resultado)
				return true;
			else
				return false;

			parent::desconectar();
		}

		public function menu()
		{
			echo '
			<ul class="navbar-nav mr-auto">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="casos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Casos
					</a>
					<div class="dropdown-menu" aria-labelledby="casos">
						<a class="dropdown-item" href="'.URL.'nuevo-caso">Nuevo</a>
						<a class="dropdown-item" href="'.URL.'casos-pendientes">Pendientes</a>
						<a class="dropdown-item" href="'.URL.'casos-abiertos">Abiertos</a>
						<a class="dropdown-item" href="'.URL.'casos-cerrados">Cerrados</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="'.URL.'casos-charts/tipo-barra">Estadísticas</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Comité
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="'.URL.'comite-integrantes">Integrantes</a>
						<a class="dropdown-item" href="'.URL.'documentos">Documentos</a>
					</div>
				</li>
				<!--li class="nav-item">
					<a class="nav-link" href="'.URL.'mensajes">Mensajes</a>
				</li-->
				<li class="nav-item">
					<a class="nav-link" href="'.URL.'cuenta">Cuenta</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="'.URL.'ayuda">Ayuda</a>
				</li>
			</ul>
			';
		}

		public function select_oficinas($pais)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$usuario = $this->info_usuario($_SESSION['codUsuario']);

			$query = "SELECT codOl, oficina FROM tbl_oficinas_locales WHERE codPais = ".$pais;

			$resultado = $conexion->query($query);

			while ($fila = $resultado->fetch_assoc())
			{
				if ($usuario['oficina'][0] == $fila['oficina'])
				{
					echo '<option value="'.$fila['codOl'].'" selected>'.$fila['oficina'].'</option>';
				}
				else
				{
					echo '<option value="'.$fila['codOl'].'">'.$fila['oficina'].'</option>';
				}
			}

			parent::desconectar();
		}

		public function select_paises()
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$usuario = $this->info_usuario($_SESSION['codUsuario']);

			$query = "SELECT * FROM tbl_paises";

			$resultado = $conexion->query($query);

			while ($fila = $resultado->fetch_assoc())
			{
				if ($usuario['codPais'][0] == $fila['codPais'])
				{
			    	echo '<option value="'.$fila['codPais'].'" selected>'.$fila['pais'].'</option>';
			    }
			    else
			    {
			    	echo '<option value="'.$fila['codPais'].'">'.$fila['pais'].'</option>';
			    }
			}

			parent::desconectar();
		}

		public function info_usuario($codUsuario)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$resultado = $conexion->query("CALL SP_infoUsuario(".$codUsuario.")");

			if ($resultado->num_rows > 0)
			{
				$usuario = [];

				while ($fila = $resultado->fetch_assoc())
				{
					$usuario['nombres'][] = $fila['nombres'];
					$usuario['apellidos'][] = $fila['apellidos'];
					$usuario['cargo'][] = $fila['cargo'];
					$usuario['depto'][] = $fila['depto'];
					$usuario['usuario'][] = $fila['usuario'];
					$usuario['email'][] = $fila['email'];
					$usuario['oficina'][] = $fila['oficina'];
					$usuario['permiso'][] = $fila['permiso'];
					$usuario['estado'][] = $fila['estado'];
					$usuario['codPais'][] = $fila['codPais'];
					$usuario['pais'][] = $fila['pais'];
				}

				return $usuario;	
			}
			else
			{
				return false;
			}

			$resultado->free();

			parent::desconectar();
		}

		public function listar_usuarios()
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "SELECT * FROM tbl_usuarios WHERE codPermiso = 2";
			$resultado = $conexion->query($query);

			if ($resultado)
			{
				$usuarios = [];
				$cantidad = false;
				while ($row = $resultado->fetch_assoc())
				{
					$cantidad = true;
				    $usuarios['codUsuario'] = $row['codUsuario'];
				    $usuarios['nombres'] = $row['nombres'];
				    $usuarios['apellidos'] = $row['apellidos'];
				    $usuarios['depto'] = $row['depto'];
				}
			}


			parent::desconectar();
		}

		/* Metodos de sección Casos */

		public function listar_casos($tipo, $clave)
		{
			$conexion = parent::conectar();

			switch ($tipo)
			{
				case 'todo':
					$conexion->set_charset('utf8');
					$query = "CALL SP_listarCasos('WHERE r.codEstado = ? ORDER BY codRegistro', ".$clave.")";
				break;

				case 'perfil':
					$conexion->set_charset('utf8');
					$query = "CALL SP_listarCasos('WHERE codRegistro = ?', ".$clave.")";
				break;

				case 'buscar':
					$conexion->set_charset('utf8');
					$query = "CALL SP_buscar('".$_SESSION['clave-busqueda']."', ".$clave.")";
				break;

				case 'buscar-abiertos':
					$conexion->set_charset('utf8');
					$query = "CALL SP_buscar()";
				break;

				case 'buscar-cerrados':
					$conexion->set_charset('utf8');
					$query = "CALL SP_buscar()";
				break;

				case 'antiguos':
					$conexion->set_charset('utf8');
					$query = "CALL SP_listarCasosAntiguos()";
				break;

				case 'reporte-antiguos':
					$query = "CALL SP_listarCasosAntiguos()";
				break;
			}

			$resultado = $conexion->query($query);

			if ($resultado)
			{
				$datos = array();

				while ($fila = $resultado->fetch_assoc())
				{
					$datos['codigo'][] = $fila['codigo'];
					$datos['nombre'][] = $fila['nombre'];
					$datos['email'][] = $fila['email'];
					$datos['oficina'][] = $fila['oficina'];
					$datos['mensaje'][] = $fila['mensaje'];

					if ($tipo != 'antiguos' && $tipo != 'reporte-antiguos')
					{
						$datos['asunto'][] = $fila['asunto'];
						$datos['estado'][] = $fila['estado'];
						$datos['tipoRegistro'][] = $fila['tipoRegistro'];
						$datos['fechaApertura'][] = $fila['fechaApertura'];
						$datos['fechaCierre'][] = $fila['fechaCierre'];
						$datos['codOpen'][] = $fila['codOpen'];
						$datos['codClose'][] = $fila['codClose'];
					}
					
					$datos['fechaRegistro'][] = $fila['fechaRegistro'];
					$datos['caso'][] = $fila['caso'];
				}

				return $datos;
			}
			else
			{
				return false;
			}

			$resultado->free();

			parent::desconectar();
		}

		public function busqueda($tipo, $clave)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			switch (expr) 
			{
				case 'pendientes':
					$query = "CALL SP_busqueda()";
				break;
			}

			parent::desconectar();
		}

		public function listar_entradas($codRegistro)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$resultado = $conexion->query("CALL SP_listarEntradas(".$codRegistro.")");

			$entradas = [];

			if ($resultado && $resultado->num_rows > 0)
			{
				while ($fila = $resultado->fetch_assoc())
				{
					$entradas['codEntrada'][] = $fila['codEntrada'];
				    $entradas['titulo'][] = $fila['titulo'];
				    $entradas['descEntrada'][] = $fila['descEntrada'];
				    $entradas['nombreDoc'][] = $fila['nombreDoc'];
				    $entradas['rutaDoc'][] = $fila['rutaDoc'];
				    $entradas['fecha'][] = $fila['fecha'];
				    $entradas['responsable'][] = $fila['responsable'];
				}

				$resultado->free();
			}

			if (count($entradas) > 0)
				return $entradas;
			else
				return false;

			parent::desconectar();
		}

		public function historial_entrada($codEntrada)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "CALL SP_historial_entrada(".$codEntrada.")";

			$resultado = $conexion->query($query);

			$historial = [];

			while ($fila = $resultado->fetch_assoc())
			{
			    $historial['codHistorial'][] = $fila['codHistorial'];
			    $historial['titulo'][] = $fila['titulo'];
			    $historial['descEntrada'][] = $fila['descEntrada'];
			    $historial['nombreDoc'][] = $fila['nombreDoc'];
			    $historial['rutaDoc'][] = $fila['rutaDoc'];
			    $historial['fecha'][] = $fila['fecha'];
			    $historial['nombre'][] = $fila['nombre'];
			}

			return $historial;

			parent::desconectar();
		}

		public function subirArchivo($codRegistro, $codEntrada, $archivo, $nombre)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$ruta = APP.'/config/data/files/'.$codRegistro.'/';

			if (!is_dir($ruta))
			{
				$crearRuta = mkdir($ruta, 0777, true);
				if (!$crearRuta)
				{
					return false;
				}
			}

			if (file_exists($ruta.$archivo))
			{
				setcookie('fail', 'El archivo cargado ya existe en los registros, por favor intente nuevamente con un documento diferente.');
			}
			else
			{
				opendir($ruta);
				$destino = $ruta.$archivo;

				if (move_uploaded_file($_FILES['archivo']['tmp_name'], $destino))
				{
					$destino = substr($destino, 28);

					$query = "CALL SP_ingresarDocumento(".$codEntrada.",'".$nombre."', '".$destino."', ".$_SESSION['codUsuario'].")";

					$resultado = $conexion->query($query);

					if ($resultado)
						return true;
					else
						return false;
				}
				else
				{
					return false;
				}
			}

			parent::desconectar();
		}

		public function subirDoc($nombreDoc, $archivo)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$ruta = APP.'/config/data/files/documentos/';

			if (!is_dir($ruta))
			{
				$crearRuta = mkdir($ruta, 0777, true);
				if (!$crearRuta)
				{
					return false;
				}
			}

			if (file_exists($ruta.$archivo))
			{
				setcookie('fail', 'El archivo cargado ya existe en los registros, por favor intente nuevamente con un documento diferente o un nombre distinto.');
			}
			else
			{
				opendir($ruta);
				$destino = $ruta.$archivo;

				if (move_uploaded_file($_FILES['file']['tmp_name'], $destino))
				{
					$destino = substr($destino, 28);

					$query = "CALL SP_newDoc('".$nombreDoc."', '".$destino."', ".$_SESSION['codUsuario'].")";

					$resultado = $conexion->query($query);

					if ($resultado)
						return true;
					else
						return false;
				}
				else
				{
					return false;
				}
			}

			parent::desconectar();
		}

		public function descargar_archivo($codDoc, $tipo)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$resultado = $conexion->query("CALL SP_verDocumento(".$codDoc.", ".$tipo.")");

			while ($data = $resultado->fetch_assoc()) { $ruta = $data['rutaDoc']; }

			$documento = $ruta;
			$documento = substr($documento, 22);
			$n = strpos($documento, "/");
			$documento = substr($documento, $n+1);

			$file = file($ruta);
			header ("Content-Type: application/force-download");
			header("Content-Disposition: attachment; filename = ".$documento);

			parent::desconectar();
		}

		public function reportes($tipo_reporte)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf-8');

			switch ($tipo_reporte)
			{
				case 'caso-cerrado':
					//-----------------------------------------------------
					$codigo = substr($_SESSION['codigoCaso'], 4);
					//-----------------------------------------------------
					$perfil = $this->listar_casos('perfil', $codigo);
					//-----------------------------------------------------
					$usuario1 = $this->info_usuario($perfil['codOpen'][0]);
					$responsable1 = $usuario1['nombres'][0].' '.$usuario1['apellidos'][0];
					//-----------------------------------------------------
					$usuario2 = $this->info_usuario($perfil['codClose'][0]);
					$responsable2 = $usuario2['nombres'][0].' '.$usuario2['apellidos'][0];
					//-----------------------------------------------------
					$entradas = $this->listar_entradas($codigo);
					$nEntradas = count($entradas['titulo']);
					//-----------------------------------------------------
					
					$html = '
					<!DOCTYPE html>
					<html lang="es-SV" dir="ltr">
						<head>
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
							<title>Caso-'.$_SESSION['codigoCaso'].'</title>
							<link rel="stylesheet" href="css/bootstrap.css">
						</head>
						<body class="bg-white">
							<div class="container mt-5">
								<div class="row">
									<div class="col-10 mb-4">
										<h1 class="display-4">
											Comité de quejas y sugerencias
										</h1>
									</div>
									<div class="col-2 mb-4">
										<img src="img/logo.png" width="125" height="100" class="float-right">
									</div>
								</div>
								<div class="row">
									<div class="col-12 mb-4">
										<h1>
											Caso '.$_SESSION['codigoCaso'].'
										</h1>
									</div>
								</div>
								<div class="row">
									<div class="col-3 float-left bg-light-gray">
										<h5 class="mt-2">Tipo de caso</h5>
										<p>'.$perfil['caso'][0].'</p>
									</div>
									<div class="col-3 float-left bg-light-gray">
										<h5 class="mt-2">Tipo de registro</h5>
										<p>'.$perfil['tipoRegistro'][0].'</p>
									</div>
									<div class="col-3 float-left bg-light-gray">
										<h5 class="mt-2">Fecha del registro</h5>
										<p>'.$perfil['fechaRegistro'][0].'</p>
									</div>
									<div class="col-3 float-left bg-light-gray">
										<h5 class="mt-2">Estado</h5>
										<p>'.$perfil['estado'][0].'</p>
									</div>
								</div>
								<div class="row">
									<div class="col-3  float-left">
										<h5 class="mt-2">Asunto</h5>
										<p>'.$perfil['asunto'][0].'</p>
									</div>
									<div class="col-3 float-left">
										<h5 class="mt-2">Nombre</h5>
										<p>'.$perfil['nombre'][0].'</p>
									</div>
									<div class="col-3 float-left">
										<h5 class="mt-2">Email</h5>
										<p>'.$perfil['email'][0].'</p>
									</div>
									<div class="col-3 float-left">
										<h5 class="mt-2">Oficina</h5>
										<p>'.$perfil['oficina'][0].'</p>
									</div>
								</div>
								<div class="row">
									<div class="col-12 float-left bg-light-gray">
										<h5 class="mt-2">Mensaje</h5>
										<p>'.$perfil['mensaje'][0].'</p>
									</div>
								</div>
								<div class="row">
									<div class="col-3 float-left">
										<h5 class="mt-2">Caso abierto por</h5>
										<p>'.$responsable1.'</p>
									</div>
									<div class="col-3 float-left">
										<h5 class="mt-2">Caso cerrado por</h5>
										<p>'.$responsable2.'</p>
									</div>
									<div class="col-3 float-left">
										<h5 class="mt-2">Fecha de apertura</h5>
										<p>'.$perfil['fechaApertura'][0].'</p>
									</div>
									<div class="col-3 float-left">
										<h5 class="mt-2">Fecha de cierre</h5>
										<p>'.$perfil['fechaCierre'][0].'</p>
									</div>
									<div class="col-4 float-left"></div>
								</div>
								<div class="row">
									<div class="col-12"></div>
								</div>';

						for ($i = 0; $i < $nEntradas; $i++)
						{
								$html .= '
								<div class="row">';
								if ($i == 0)
								{
									$html .= '
									<div class="col-12">
										<hr class="bg-dark">
									</div>';
								}
									$html .= '
									<div class="col-12">
										<h3>'.$entradas['titulo'][$i].'</h3>
									</div>
								</div>
								
								<div class="row mt-1em">
									<div class="col-12 float-left text-adjust">
										<h5>Descripción de la entrada</h5>
										<p class="text-justify">'.$entradas['descEntrada'][$i].'</p>
									</div>
								</div>
								<div class="row">
									<div class="col-6 float-left">
										<h5>Documento</h5>';

										if ($entradas['rutaDoc'][$i] != '')
										{
											$html .= '<p>'.$entradas['nombreDoc'][$i].'</p>';
										}
										else
										{
											$html .= '<p>Empty</p>';
										}

										$html .= '
									</div>
									<div class="col-6 float-left">
										<h5>Fecha de entrada</h5>
										<p>'.$entradas['fecha'][$i].'</p>
									</div>
								</div>
								<div class="row">
									<div class="col-12 float-left">';
										$hEntrada = $this->historial_entrada($entradas['codEntrada'][$i]);
										$html .= '<h5>Responsables de la entrada</h5>';
										$personas = array_unique($hEntrada['nombre']);
										$n = count($personas);
										$html .= '<p>';
										$contador = 1;
										foreach ($personas as $key => $value)
										{
											if ($contador == $n)
												$html .= $value;
											else
												$html .= $value.', ';
											$contador++;
										}
										$html .= '</p>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<hr class="bg-dark">
									</div>
								</div>';
						}
						$html .= '
							<div class="row">
								<div class="col-12">
									<p class="float-right">
										<small>Este reporte ha sido generado automáticamente el día '.date('d-m-Y').'</small>
									</p>
								</div>
							</div>
							</div>
						</body>
					</html>';
				break;

				case 'reporte-antiguos':
					//-----------------------------------------------------
					$datos = $this->listar_casos('reporte-antiguos', 0);
					//-----------------------------------------------------
					$n = count($datos['codigo']);
					$fecha = date('d-M-Y');

					$html = "
						<table>
							<tr>
								<td colspan='7'><h2>Comit&eacute de quejas, sugerencias y recomendaciones</h2></td>
							</tr>
							<tr>
								<td colspan='5'><h3>Reporte de registros efectivos al ".$fecha."</h3></td>
							</tr>
							<tr></tr>
						</table>
						<table border='1' cellspacing='0'>
							<thead>
							  <tr bgcolor='#8DA593' height='50'>
							    <th>No</th>
							    <th>Tipo</th>
							    <th>Nombre</th>
							    <th>E-mail</th>
							    <th>Fecha</th>
							    <th>Centro de trabajo</th>
							    <th>Mensaje</th>
							  </tr>
							</thead>
							<tbody>";
					for ($i = 0; $i < $n; $i++)
					{
						if ($i % 2 == 0) {
							$html.= "
							  <tr valign='top' bgcolor='#E6E6E6'>
							    <td>".$datos['codigo'][$i]."</td>
							    <td>".$datos['caso'][$i]."</td>
							    <td>".$datos['nombre'][$i]."</td>
							    <td>".$datos['email'][$i]."</td>
							    <td>".$datos['fechaRegistro'][$i]."</td>
							    <td>".$datos['oficina'][$i]."</td>
							    <td>".$datos['mensaje'][$i]."</td>
							  </tr>";
						}else{
							$html.= "
								  <tr valign='top'>
								    <td>".$datos['codigo'][$i]."</td>
								    <td>".$datos['caso'][$i]."</td>
								    <td>".$datos['nombre'][$i]."</td>
								    <td>".$datos['email'][$i]."</td>
								    <td>".$datos['fechaRegistro'][$i]."</td>
								    <td>".$datos['oficina'][$i]."</td>
								    <td>".$datos['mensaje'][$i]."</td>
								  </tr>";
						}
					}
							$html.= "
							</tbody>
						</table> ";
				break;
			}

			return $html;

			parent::desconectar();
		}

		public function charts()
		{
			$conexion = parent::conectar();
			$fecha = date('Y-m-d');
			$query = "CALL SP_charts('{$fecha}')";

			$resultado = $conexion->query($query);

			$datos = [
				'allPersonal' => [ //Todos los Personales
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allAnonimous' => [//Todos los Anonimos 
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allP' => [ //Todos los Pendientes
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allA' => [ //Todos los Abiertos
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allC' => [ //Todos los Cerrados
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allQ' => [ //Todas las quejas
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allS' => [ //Todas las sugerencias
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'allR' => [ //Todos los reconocimientos
					'Enero' => [0],
					'Febrero' => [0],
					'Marzo' => [0],
					'Abril' => [0],
					'Mayo' => [0],
					'Junio' => [0],
					'Julio' => [0],
					'Agosto' => [0],
					'Septiembre' => [0],
					'Octubre' => [0],
					'Noviembre' => [0],
					'Diciembre' => [0]
				],

				'quejas' => [0], 'sugerencias' => [0], 'reconocimientos' => [0],

				'Pendientes' => [0], 'Abiertos' => [0], 'Cerrados' => [0],

				'Personales' => [0], 'Anonimos' => [0]
			];

			while ($fila = $resultado->fetch_assoc())
			{
			    switch ($fila['mes'])
			    {
			    	case '01':

					  	switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Enero'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Enero'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Enero'][0]++;
					    	break;
					    }

					  	if ($fila['tipoRegistro'] == 'Personal')
					  	{
				    		$datos['allPersonal']['Enero'][0]++;
				    	}  	
				    	else
				    	{
				    		$datos['allAnonimous']['Enero'][0]++;
				    	}

				    	switch ($fila['codEstado'])
				    	{
				    		case '3':
				    			$datos['allP']['Enero'][0]++;
				    		break;

				    		case '4':
				    			$datos['allA']['Enero'][0]++;
				    		break;

				    		case '5':
				    			$datos['allC']['Enero'][0]++;
				    		break;
				    	}

			    	break;

					case '02':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Febrero'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Febrero'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Febrero'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Febrero'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Febrero'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Febrero'][0]++;
							break;

							case '4':
								$datos['allA']['Febrero'][0]++;
							break;

							case '5':
								$datos['allC']['Febrero'][0]++;
							break;
						}

					break;

					case '03':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Marzo'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Marzo'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Marzo'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Marzo'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Marzo'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Marzo'][0]++;
							break;

							case '4':
								$datos['allA']['Marzo'][0]++;
							break;

							case '5':
								$datos['allC']['Marzo'][0]++;
							break;
						}

					break;

					case '04':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Abril'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Abril'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Abril'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Abril'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Abril'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Abril'][0]++;
							break;

							case '4':
								$datos['allA']['Abril'][0]++;
							break;

							case '5':
								$datos['allC']['Abril'][0]++;
							break;
						}

					break;

					case '05':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Mayo'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Mayo'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Mayo'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Mayo'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Mayo'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Mayo'][0]++;
							break;

							case '4':
								$datos['allA']['Mayo'][0]++;
							break;

							case '5':
								$datos['allC']['Mayo'][0]++;
							break;
						}

					break;

					case '06':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Junio'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Junio'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Junio'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Junio'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Junio'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Junio'][0]++;
							break;

							case '4':
								$datos['allA']['Junio'][0]++;
							break;

							case '5':
								$datos['allC']['Junio'][0]++;
							break;
						}

					break;

					case '07':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Julio'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Julio'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Julio'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Julio'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Julio'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Julio'][0]++;
							break;

							case '4':
								$datos['allA']['Julio'][0]++;
							break;

							case '5':
								$datos['allC']['Julio'][0]++;
							break;
						}

					break;

					case '08':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Agosto'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Agosto'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Agosto'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Agosto'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Agosto'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Agosto'][0]++;
							break;

							case '4':
								$datos['allA']['Agosto'][0]++;
							break;

							case '5':
								$datos['allC']['Agosto'][0]++;
							break;
						}

					break;

					case '09':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Septiembre'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Septiembre'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Septiembre'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Septiembre'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Septiembre'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Septiembre'][0]++;
							break;

							case '4':
								$datos['allA']['Septiembre'][0]++;
							break;

							case '5':
								$datos['allC']['Septiembre'][0]++;
							break;
						}

					break;

					case '10':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Octubre'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Octubre'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Octubre'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Octubre'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Octubre'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Octubre'][0]++;
							break;

							case '4':
								$datos['allA']['Octubre'][0]++;
							break;

							case '5':
								$datos['allC']['Octubre'][0]++;
							break;
						}

					break;

					case '11':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Noviembre'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Noviembre'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Noviembre'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Noviembre'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Noviembre'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Noviembre'][0]++;
							break;

							case '4':
								$datos['allA']['Noviembre'][0]++;
							break;

							case '5':
								$datos['allC']['Noviembre'][0]++;
							break;
						}

					break;

					case '12':

						switch ($fila['caso'])
					    {
					    	case 'Queja':
					    		$datos['allQ']['Diciembre'][0]++;
					    	break;
					    	
					    	case 'Sugerencia':
					    		$datos['allS']['Diciembre'][0]++;
					    	break;

					    	case 'Reconocimiento':
					    		$datos['allR']['Diciembre'][0]++;
					    	break;
					    }

						if ($fila['tipoRegistro'] == 'Personal')
						{
							$datos['allPersonal']['Diciembre'][0]++;
						}
						else
						{
							$datos['allAnonimous']['Diciembre'][0]++;
						}

						switch ($fila['codEstado'])
						{
							case '3':
								$datos['allP']['Diciembre'][0]++;
							break;

							case '4':
								$datos['allA']['Diciembre'][0]++;
							break;

							case '5':
								$datos['allC']['Diciembre'][0]++;
							break;
						}

					break;
			    }

			    switch ($fila['codEstado'])
			    {
			    	case '3':
			    		$datos['Pendientes'][0]++;
			    	break;
			    	
			    	case '4':
			    		$datos['Abiertos'][0]++;
			    	break;

			    	case '5':
			    		$datos['Cerrados'][0]++;
			    	break;
			    }

			    switch ($fila['caso'])
			    {
			    	case 'Queja':
			    		$datos['quejas'][0]++;
			    	break;
			    	
			    	case 'Sugerencia':
			    		$datos['sugerencias'][0]++;
			    	break;

			    	case 'Reconocimiento':
			    		$datos['reconocimientos'][0]++;
			    	break;
			    }

			    if ($fila['tipoRegistro'] == 'Personal')
			  	{
		    		$datos['Personales'][0]++;
		    	}  	
		    	else
		    	{
		    		$datos['Anonimos'][0]++;
		    	}
			}

			$resultado->free();

			return $datos;

			parent::desconectar();
		}

		/* Metodos de seccion del comite */

		public function propuestas()
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "SELECT * FROM tbl_propuestas";

			$resultado = $conexion->query($query);

			if ($resultado)
			{
				$propuestas = [];
				$num = false;
				while ($row = $resultado->fetch_assoc())
				{
					$num = true;
				    $propuestas['codPropuesta'][] = $row['codPropuesta'];
				    $propuestas['titulo'][] = $row['titulo'];
				    $propuestas['descripcion'][] = $row['descripcion'];
				    $propuestas['fecha'][] = $row['fecha'];
				    $propuestas['codUsuario'][] = $row['codUsuario'];
				}

				if ($num)
					return $propuestas;
				else
					return false;
			}
			else
			{
				return false;
			}
		}

		public function infoPropuesta($codPropuesta)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "SELECT * FROM tbl_propuestas WHERE codPropuesta = ".$codPropuesta;

			$resultado = $conexion->query($query);

			if ($resultado)
			{
				while ($row = $resultado->fetch_assoc())
				{
					echo '
					<div class="row h-3em"></div>
					<div class="row">
						<div class="col-12">
							<h1 class="display-4"><i class="fas fa-file-invoice"></i> Información de propuesta</h1>
						</div>
					</div>
					<div class="row mt-4">
						<div class="col-12 col-lg-6 d-col-lg float-left">
							<h5 class="mt-2">Titulo</h5>
							<p>'.$row['titulo'].'</p>
						</div>
						<div class="col-12 col-lg-6 d-col-lg float-left">
							<h5 class="mt-2">Fecha de registro</h5>
							<p>'.$row['fecha'].'</p>
						</div>
					</div>
					<div class="row">
						<div class="col-12 text-justify">
							<h5 class="mt-2">Descripción</h5>
							<p>'.$row['descripcion'].'</p>
						</div>
					</div>
					<div class="row mt-4">
						<div class="col-12">
							<a class="btn btn-dark text-white" href="'.URL.'comite-propuestas">
								<i class="fas fa-backward"></i> Volver
							</a>
						</div>
					</div>
					<div class="row h-3em"></div>';
				}
			}

			parent::desconectar();
		}

		public function listarDocumentos()
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "
			SELECT
				d.codDoc,
				d.nombreDoc,
		        CONCAT(DATE_FORMAT(d.fecha, '%d'), '/', DATE_FORMAT(d.fecha, '%m'), '/', DATE_FORMAT(d.fecha, '%Y')) AS 'fecha',
		        CONCAT(u.nombres, ' ', u.apellidos) AS 'responsable'
			FROM 
				tbl_documentos d
			INNER JOIN
				tbl_usuarios u ON d.codUsuario = u.codUsuario;
			";

			$resultado = $conexion->query($query);

			$i = 1;

			while ($fila = $resultado->fetch_assoc())
			{
			    echo '
			    <div class="alert alert-light text-dark">
					<table class="w-100">
						<tbody>
							<tr>
								<td width="10%">'.$i.'</td>
								<td width="30%">'.$fila['nombreDoc'].'</td>
								<td width="20%">'.$fila['fecha'].'</td>
								<td width="30%">'.$fila['responsable'].'</td>
								<td width="10%">
									<form action="'.URL.'" method="post" accept-charset="utf-8" enctype="multipart/form-data">
										<button type="submit" name="descargarDoc" class="btn btn-dark" value="'.$fila['codDoc'].'">
											<i class="fas fa-download"></i> Descargar
										</button>
									</form>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			    ';

			    $i++;
			}

			parent::desconectar();
		}

		public function verificarUsuario($user)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "SELECT usuario FROM tbl_usuarios WHERE usuario = '".$user."'";

			$resultado = $conexion->query($query);

			$valor = '';

			while ($fila = $resultado->fetch_assoc())
			{
			    $valor = $fila['usuario'];
			}

			if ($valor != $user)
				return true; //'<span class="text-success ml-5">Usuario disponible.</span>';
			else
				return false; //'<span class="text-danger ml-5">Usuario no disponible.</span>';

			parent::desconectar();
		}

		public function update_user($tipo, $nombres, $apellidos, $cargo, $depto, $email, $office, $pais, $user, $pass, $pass2)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			if ($tipo == 'person')
			{
				$query = "
					UPDATE 
						`tbl_usuarios` 
					SET 
						`nombres` = '".$nombres."',
						`apellidos` = '".$apellidos."',
						`cargo` = '".$cargo."',
						`depto` = '".$depto."',
						`email` = '".$email."',
						`codOl` = ".$office.",
						`codPais` = ".$pais."
					WHERE
						`codUsuario` = ".$_SESSION['codUsuario'];

				$resultado = $conexion->query($query);

				if ($resultado)
				{
					setcookie('success', 'Los datos han sido actualizados exitosamente.');
				}
				else
				{
					setcookie('fail', 'Surgió un problema al enviar los datos, intentalo más tarde.');
				}
			}
			else
			{
				$passCrypt = md5(sha1(md5($pass)));
				$query = "SELECT * FROM tbl_usuarios WHERE pass = '".$passCrypt."'";

				$result = $conexion->query($query);

				if ($result->num_rows != 0)
				{
					if ($pass == $pass2)
					{
						if($this->verificarUsuario($user))
						{
							$query = "
								UPDATE 
									`tbl_usuarios` 
								SET 
									`usuario` = '".$user."'
								WHERE
									`codUsuario` = ".$_SESSION['codUsuario'];

							$resultado = $conexion->query($query);

							if ($resultado)
							{
								if (isset($_SESSION['txt_user'])) { unset($_SESSION['txt_user']); }
								setcookie('success', 'El usuario ha sido actualizado exitosamente.');
							}
							else
							{
								setcookie('fail', 'Surgió un problema al enviar los datos, intentalo más tarde.');
							}
						}
						else
						{
							$_SESSION['txt_user'] = $user;
							setcookie('fail', 'El usuario <strong>'.$user.'</strong> se encuentra disponible.');
						}
					}
					else
					{
						$_SESSION['txt_user'] = $user;
						setcookie('fail', 'Las contraseñas ingresadas no coinciden, intentalo nuevamente.');
					}
				}
				else
				{
					setcookie('fail', 'Las contraseña ingresada es incorrecta.');
				}

			}

			parent::desconectar();
		}

		public function updatePass($pass, $pass2)
		{
			if ($pass == $pass2)
			{
				$passCrypt = md5(sha1(md5($pass)));

				$query = "UPDATE tbl_usuarios SET pass = '".$passCrypt."' WHERE codUsuario = ".$_SESSION['codUsuario'];

				if ($this->execute_query($query))
				{
					setcookie('success', 'La contraseña ha sido actualizada exitosamente.');
				}
				else
				{
					setcookie('fail', 'Surgió un problema al enviar los datos, intentalo más tarde.');
				}
			}
			else
			{
				setcookie('fail', 'Las contraseñas no coinciden');
			}
		}

		public function listar_integrantes()
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');

			$query = "
			SELECT
		        u.nombres,
		        u.apellidos,
		        u.email,
		        u.cargo,
		        u.depto,
		        o.oficina
			FROM
				tbl_usuarios u
			JOIN
				tbl_oficinas_locales o ON u.codOl = o.codOl
			WHERE
				codPermiso = 3
			";

			$resultado = $conexion->query($query);

			$i = 1;
			while ($fila = $resultado->fetch_assoc())
			{
			    echo '
				<tr>
					<td>'.$i.'</td>
					<td>'.$fila['nombres'].' '.$fila['apellidos'].'</td>
					<td>'.$fila['email'].'</td>
					<td>'.$fila['depto'].'</td>
					<td>'.$fila['cargo'].'</td>
					<td>'.$fila['oficina'].'</td>
				</tr>	
			    ';
			    $i++;
			}

			parent::desconectar();
		}

		private function sendMail($nombre, $email, $asunto, $html)
		{
			$conexion = parent::conectar();
			$conexion->set_charset('utf8');
			
			$mensaje = $html;
			$mail = new PHPMailer;
			//indico a la clase que use SMTP
			$mail->IsSMTP();
			//permite modo debug para ver mensajes de las cosas que van ocurriendo
			//$mail->SMTPDebug = 2;
			//Debo de hacer autenticación SMTP
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "tls"; //ssl
			//indico el servidor de Gmail para SMTP
			$mail->Host = "smtp.office365.com"; //smtp.gmail.com
			//indico el puerto que usa Gmail
			$mail->Port = 587; //465 - gmail
			//indico un usuario / clave de un usuario de gmail
			$mail->Username = "educo@educo.sv";
			$mail->Password = "$3duco2018";
			$mail->CharSet = "UTF-8";
			$mail->From = "educo@educo.sv";
			$mail->FromName = "Sistemas - Educo El Salvador";
			$mail->Subject = $asunto;
			$mail->addAddress($email, $nombre);
			$mail->MsgHTML($mensaje);

			if($mail->Send()){
				return true;
			}else{
				return false;
			}
			parent::desconectar();	
		}
	}
?>