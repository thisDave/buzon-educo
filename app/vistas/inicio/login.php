<!DOCTYPE html>
<html lang="es-SV" dir="ltr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php echo SITIO; ?></title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/animate.css">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<!--Icon-->
  		<link rel="shortcut icon" type="image/x-icon" href="img/icono.ico">

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/style.css">
		
	</head>
	<body class="bg-white">
		<header>			
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-white">
				<a class="navbar-brand text-dark-green" href="<?= URL ?>">EDUCO SV</a>
				<button class="navbar-toggler bg-dark-green" type="button" data-toggle="collapse" data-target="#contenido" aria-controls="contenido" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="contenido">
					<ul class="navbar-nav mr-auto"></ul>
					<form action="<?php echo URL; ?>" method="post" class="form-inline my-2 my-lg-0">					
				    	<a class="btn btn-danger text-white float-right" href="<?= URL ?>inicio">
				    		<i class="fas fa-backward"></i> Volver
				    	</a>
				    </form>

				</div>
			</nav>
		</header>

		<div class="container mt-5">
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<div class="text-center">
					  	<img src="img/logo.png" class="rounded" width="170" height="120">
					</div>
					<form class="form-signin" action="<?php echo URL; ?>" method="post" accept-charset="utf-8">
						<h1 class="h3 mb-3 font-weight-normal text-center">Iniciar Sesión</h1>
						<label for="usuario" class="sr-only">Usuario</label>
						<input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" autocomplete="off" required autofocus>
						<label for="password" class="sr-only">Contraseña</label>
						<input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" required>
						<label>
						  <a class="btn btn-link" data-toggle="modal" data-target="#forget">
						  	<i class="fas fa-search"></i> Olvidé mi contraseña
						  </a>
						</label>
						<button class="btn btn-lg btn-info btn-block" type="submit" name="login">
							Iniciar Sesión
						</button>
						<p class="mt-3 mb-3 text-muted text-center">&copy; Fundación Educo SV</p>
				    </form>
				    <?php if (isset($_SESSION['error_login'])): ?>
				    	<p class="text-center text-danger"><?php echo $_SESSION['error_login']; ?></p>
				    <?php endif ?>
				</div>
				<div class="col-lg-4"></div>
			</div>
		</div>	
		
		<!-- Modal Forget -->
		<div class="modal fade" id="forget" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header bg-dark-green text-white">
						<h5 class="modal-title" id="modalForget"><i class="fas fa-key"></i> Recuperar contraseña</h5>
					</div>
					<form action="<?= URL ?>" method="post" accept-charset="utf-8">
						<div class="modal-body">
							<div class="form-group">
								<label for="email">Correo Electrónico:</label>
								<input type="email" name="email" class="form-control" autocomplete="off" placeholder="nombre.apellido@educo.org" aria-discribedby="emailHelp" required>
								<small id="emailHelp" class="form-text text-muted">Ingresa el correo electrónico asociado a tu cuenta.</small>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn bg-dark-green text-white" name="btnForget">
								<i class="fas fa-unlock-alt"></i> Recuperar contraseña
							</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fas fa-times"></i> Cerrar
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

<?php require_once APP.'/vistas/include/footer.php'; ?>