<!DOCTYPE html>
<html lang="es-SV" dir="ltr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php echo SITIO; ?></title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<!--Icon-->
  		<link rel="shortcut icon" type="image/x-icon" href="img/icono.ico">

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/style.css">
		
	</head>
	<body class="bg-white">
		<header>			
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-white">
				<a class="navbar-brand text-dark-green" href="<?= URL ?>">EDUCO SV</a>
				<button class="navbar-toggler bg-dark-green" type="button" data-toggle="collapse" data-target="#contenido" aria-controls="contenido" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="contenido">
					<ul class="navbar-nav mr-auto"></ul>
					<form action="<?php echo URL; ?>" method="post" class="form-inline my-2 my-lg-0">					
				    	<a class="btn bg-dark-green text-white my-2 my-sm-0" href="<?= URL ?>login">
				    		<i class="fas fa-sign-in-alt"></i> Iniciar Sesión
				    	</a>
				    </form>

				</div>
			</nav>
		</header>

		<div class="container mt-5">
			<div class="row">
				<div class="col-1"></div>
				<div class="col-10">
					<div class="container mb-4">
						<div class="text-center">
						  <img src="img/logo.png" class="rounded" width="170" height="120">
						</div>
						<h1 class="display-4 text-center">Buzón Educo</h1>
						<h5 class="text-center">Por favor llena los datos solicitados para que podamos atenderte lo más rápido posible.</h5>

						<div class="form-row mt-5 text-center">
							<div class="form-group col-12">
								<a class="btn bg-dark-green text-white" href="<?= URL ?>version_completa">
									<i class="fas fa-user-alt"></i> Versión Completa
								</a>
								<a class="btn btn-dark text-white" href="<?= URL ?>version_anonima">
									<i class="fas fa-user-secret"></i> Versión Anónima
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>

<?php require_once 'app/vistas/include/footer.php'; ?>