<!DOCTYPE html>
<html lang="es-SV" dir="ltr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title><?php echo SITIO; ?></title>

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">

		<!--Icon-->
  		<link rel="shortcut icon" type="image/x-icon" href="img/icono.ico">

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" href="css/animate.css">

		<!-- Fonts Google -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

		<!--Data Range Picker-->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	</head>
	<body>

	<body class="bg-white">

		<div class="container">
			<div class="row">
				<div class="col-1">
					<img src="img/logo.png" class="rounded float-left" width="170" height="120" style="margin-left: -15px;">
				</div>
				<div class="col-11">
					<h1 class="display-5 text-center mt-4"><strong>Buzón de quejas, sugerencias y reconocimientos</strong></h1>
					<p class="text-center">Por favor llena los datos solicitados para que podamos atenderte lo más rápido posible.</p>
				</div>
				<div class="col-12">
					<form class="mt-3" action="<?php echo URL; ?>" method="post" accept-charset="utf8">
						<div class="form-row">
							<div class="form-group col-4">
								<label for="nombreCompleto">Nombre completo:</label>
								<input type="text" class="form-control" id="nombreCompleto" name="nombre" placeholder="nombres y apellidos" pattern="[A-Za-z]{5,60}+" autocomplete="off" required>
							</div>
							<div class="form-group col-4">
								<label for="ol">Oficina local:</label>
								<select name="ol" id="ol" class="form-control" required>
									<option value="">Selecciona tu oficina local</option>
									<option value="2">Región San Salvador</option>
									<option value="1">Región La Libertad</option>
									<option value="3">Región San Vicente</option>
									<option value="4">Región San Miguel</option>
								</select>
							</div>
							<div class="form-group col-4">
								<label for="correo">Correo institucional:</label>
								<div class="input-group">
									<input type="text" class="form-control" id="correo" name="correo" aria-label="Recipient's username" aria-describedby="emailHelp basic-addon2" placeholder="Ejemplo: isaac.ramos" autocomplete="off" required>
									<div class="input-group-append">
										<span class="input-group-text" id="basic-addon2">@educo.org</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-row" style="margin-top: -15px;">
							<div class="form-group col-12">
								<small id="emailHelp" class="form-text text-muted">Por seguridad la información de tu correo institucional será utilizada de manera confidencial, estarémos comunicandonos contigo más adelante.</small>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-7">
								<label for="asunto">Asunto:</label>
								<input type="text" class="form-control" id="asunto" name="asunto" aria-describedby="asuntoHelp" placeholder="Escribe el asunto de tu mensaje" autocomplete="off" required>
								<small id="asuntoHelp" class="form-text text-muted">Escribe brevemente el asunto de tu queja, sugerencia o reconocimiento.</small>
							</div>
							<div class="form-group col-5">
								<label for="tipo">Tipo de caso:</label>
								<select name="tipo" id="tipo" class="form-control" required>
									<option value="">Selecciona un caso</option>
									<option value="1">Queja</option>
									<option value="2">Sugerencia</option>
									<option value="3">Felicitaciones</option>
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-12">
								<label for="descripcion">Describe brevemente el caso:</label>
								<textarea class="form-control" id="descripcion" wrap="soft" name="mensaje" rows="5" placeholder="Cantidad de caracteres permitidos: 1,000" maxlength="10000" required></textarea>
							</div>
						</div>
						<button type="submit" name="registroCompleto" class="btn bg-dark-green text-white">
							<i class="fas fa-edit"></i> Enviar Caso
						</button>
						<a class="btn btn-danger text-white float-right" href="<?= URL ?>inicio">
				    		<i class="fas fa-backward"></i> Volver
				    	</a>
					</form>
				</div>
			</div>
		</div>

		<?php if(isset($_COOKIE['success'])): ?>

		<div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-lg" role="document">
				<div class="modal-content bg-dark-green">
					<div class="modal-header text-white">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="text-white">&times;</span>
						</button>
					</div>
					<div class="modal-body bg-light">
						<p class="display-6"><?php  echo $_COOKIE['success']; ?> &nbsp; <img src="img/success.gif" width="35" height="35"></p>
					</div>
					<div class="modal-footer bg-light">
						<button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>

		<?php if(isset($_COOKIE['fail'])): ?>

		<div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-lg" role="document">
				<div class="modal-content bg-dark-green">
					<div class="modal-header text-white">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="text-white">&times;</span>
						</button>
					</div>
					<div class="modal-body bg-light">
						<p class="display-6"><i class="fas fa-times-circle text-danger"></i> <?php  echo $_COOKIE['fail']; ?></p>
					</div>
					<div class="modal-footer bg-light">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script type="text/javascript" src="js/main.js"></script>

		<script>
			$(document).ready(function(){
		    	$("#mostrarmodal").modal("show");

		    	document.cookie="success=borrar; max-age= -1;";
		    	document.cookie="fail=borrar; max-age= -1;";

		    	setTimeout(function() { $('#mostrarmodal').modal('hide'); }, 2425);
		    });
		</script>
	</body>
</html>
