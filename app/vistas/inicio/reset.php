<?php 
if (isset($_SESSION['codUsuario']))
{	
	$usuario = $objModelo->info_usuario($_SESSION['codUsuario']);
}
else
{
	header("Location: ".URL.'login');
}
?>
<!DOCTYPE html>
<html lang="es-SV" dir="ltr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title><?php echo SITIO; ?></title>
		
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?= URL ?>css/bootstrap.css">

		<!--Icon-->
  		<link rel="shortcut icon" type="image/x-icon" href="<?= URL ?>img/icono.ico">

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="<?= URL ?>css/style.css">
		<link rel="stylesheet" href="<?= URL ?>css/animate.css">

		<!-- Fonts Google -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

		<!--Data Range Picker-->
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

		<script>
		function Habilitar()
		{
			var txt_pass1 = document.getElementById('txt_pass1');
			var txt_pass2 = document.getElementById('txt_pass2');
			var btnResetPass = document.getElementById('btnResetPass');
			var resultado = document.getElementById('resultado');

			if (txt_pass1 != "" || txt_pass2 != "")
			{
				if (txt_pass1.value != txt_pass2.value)
				{
					btnResetPass.disabled = true;
					resultado.classList.add('text-danger');
					resultado.innerHTML = 'Las contraseñas no coinciden!!';
				}
				else
				{
					resultado.classList.remove('text-danger');
					resultado.classList.add('text-success');
					btnResetPass.disabled = false;
					resultado.innerHTML = 'Las contraseñas si coinciden!!';
				}
			}
			else
			{
				btnSavePass.disabled = true;
			}
		}
	</script>
	</head>
	<body class="bg-white">

		<div class="container-fluid">
			<div class="row">
				<div class="col-1">
					<img src="<?= URL ?>img/logo.png" class="rounded float-left" width="170" height="120">
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<p class="display-6">Restablecimiento de contraseña.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-5">
					<p class="text-justify">
						<strong>Hola <?= $usuario['nombres'][0] ?> <?= $usuario['apellidos'][0] ?></strong>
						<br>
						Hemos recibido tu solicitud para restablecer tu contraseña, por favor ingresa tu nueva contraseña.
					</p>
					<p>
						Para que tu contraseña sea válida deberá ser mayor o igual a <strong>6 caracteres.</strong>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-7">
					<form class="mt-3" action="<?php echo URL; ?>" method="post" accept-charset="utf8">
						<div class="form-row">
							<div class="form-group col-4">
								<label for="txt_pass1">Ingresa tu nueva contraseña</label>
								<input type="password" name="pass" id="txt_pass1" minlength="6" class="form-control" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-4">
								<label for="txt_pass2">Repite tu nueva contraseña</label>
								<input type="password" name="pass2" id="txt_pass2" minlength="6" onkeyup="Habilitar()" class="form-control" required>
							</div>
						</div>
						<div class="form-row mt-3">
							<div class="form-group col-12">
								<button type="submit" name="resetPass" id="btnResetPass" value="<?= $usuario['codUsuario'][0] ?>" class="btn bg-dark-green text-white">
									<i class="fas fa-key"></i> Restablecer contraseña
								</button>
								&nbsp;
								<span id="resultado"></span>
							</div>
						</div>
						<div class="form-row mt-3">
							<div class="form-group col-12">
								<a class="btn btn-danger text-white" href="<?= URL ?>cancelReset">
						    		<i class="fas fa-times-circle"></i> Cancelar
						    	</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php if(isset($_COOKIE['success'])): ?>

		<div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-lg" role="document">
				<div class="modal-content bg-dark-green">
					<div class="modal-header text-white">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="text-white">&times;</span>
						</button>
					</div>
					<div class="modal-body bg-light">
						<p class="display-6"><?php  echo $_COOKIE['success']; ?> &nbsp; <img src="img/success.gif" width="35" height="35"></p>
					</div>
					<div class="modal-footer bg-light">
						<button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>

		<?php if(isset($_COOKIE['fail'])): ?>

		<div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-lg" role="document">
				<div class="modal-content bg-dark-green">
					<div class="modal-header text-white">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="text-white">&times;</span>
						</button>
					</div>
					<div class="modal-body bg-light">
						<p class="display-6"><i class="fas fa-times-circle text-danger"></i> <?php  echo $_COOKIE['fail']; ?></p>
					</div>
					<div class="modal-footer bg-light">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script type="text/javascript" src="js/main.js"></script>
		
		<script>
			$(document).ready(function(){
		    	$("#mostrarmodal").modal("show");

		    	document.cookie="success=borrar; max-age= -1;";
		    	document.cookie="fail=borrar; max-age= -1;";

		    	setTimeout(function() { $('#mostrarmodal').modal('hide'); }, 2425);
		    });
		</script>
	</body>
</html>