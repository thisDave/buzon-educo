<?php require_once APP.'/vistas/include/header.php'; ?>

	<div class="container-fluid mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-11">
				<h1 class="display-4"><i class="fas fa-hands-helping"></i> Centro de ayuda</h1>
			</div>
		</div>
		<div class="row mt-2">
			<div class="col-1"></div>
			<div class="col-11">
				<p class="display-5 text-secondary">
					¿Tienes problemas o tienes una pregunta específica para nosotros? Estamos aquí para ayudar.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-5">
				<ul class="list-group">
					<li class="list-group-item">
						<p class="display-6">¿Que es un caso?</p>
						<p class="text-justify">
							Son todas las quejas, sugerencias o reconocimientos que una persona ingresa por medio del formulario al inicio del sitio, estos casos poseen un perfil el cual contiene toda la información necesaria para crear entradas.
						</p>
					</li>
					<li class="list-group-item">
						<p class="display-6">¿Que es una entrada?</p>
						<p class="text-justify">
							Las entradas son acciones que se registran en los perfiles de cada casos, estos registros son introducidos por los integrantes del comité, de acuerdo al debido seguimiento que se lleve al respecto.
						</p>
					</li>
					<li class="list-group-item">
						<p class="display-6">¿Existe un historial de entradas por caso?</p>
						<p class="text-justify">
							Todas las entradas pueden ser editadas y/o eliminadas, sin embargo, cada caso posee un historial virtual de inserción, edición y borrado de registros, el historial de entradas únicamente puede ser observado por el administrador del sistema.
						</p>
					</li>
				</ul>
			</div>
			<div class="col-5">
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action">Manual de administración de casos.</a>
					<a href="#" class="list-group-item list-group-item-action">Pasos para enviar un caso de tipo completo.</a>
					<a href="#" class="list-group-item list-group-item-action">Pasos para enviar un caso de tipo anónimo.</a>
					<a href="#" class="list-group-item list-group-item-action">Procedimiento 1</a>
					<a href="#" class="list-group-item list-group-item-action">Procedimiento 2</a>
					<a href="#" class="list-group-item list-group-item-action">Procedimiento 3</a>
					<a href="#" class="list-group-item list-group-item-action">Procedimiento 4</a>
					<a href="#" class="list-group-item list-group-item-action">Procedimiento 5</a>
					<a href="#" class="list-group-item list-group-item-action">Procedimiento 6</a>
				</div>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-12">
				
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>