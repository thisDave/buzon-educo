<?php require_once APP.'/vistas/include/header.php'; ?>
	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4"><i class="fas fa-users"></i> Integrantes del comité</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table">
					<thead>
						<tr>
							<th>N°</th>
							<th>Nombre completo</th>
							<th>Correo electrónico</th>
							<th>Área</th>
							<th>Cargo</th>
							<th>Región</th>
						</tr>
					</thead>
					<tbody>
						<?php $obj->listar_integrantes(); ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-12">
				<table class="float-right">
					<tbody>
						<tr>
							<td>
								<a class="btn btn-success text-white float-right" data-toggle="modal" data-target="#nueva-propuesta">
									<i class="far fa-newspaper"></i> Nueva propuesta
								</a>
							</td>
							<td>
								<a class="btn btn-primary text-white float-right" href="<?php echo URL.'comite-propuestas'; ?>">
									<i class="fas fa-eye"></i> Ver propuestas
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal-->

	<div class="modal fade" id="nueva-propuesta" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h3 class="modal-title">
						<i class="far fa-edit"></i> Nueva Propuesta
					</h3>
				</div>
				<div class="modal-body">
					<form action="<?php echo URL; ?>" method="POST" accept-charset="utf-8">
						<div class="form-group">
							<label for="titulo">
								Título
							</label>
							<input type="text" id="titulo" name="titulo-propuesta" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="descripcion">
								Descripción
							</label>
							<textarea id="descripcion" name="descripcion-propuesta" class="form-control" required></textarea>
						</div>
				</div>
				<div class="modal-footer">
						<button type="submit" class="btn bg-dark-green text-white" name="guardar-propuesta">
							<i class="fas fa-check"></i> Guardar
						</button>
					</form>
					<button type="button" class="btn btn-dark" data-dismiss="modal">
						<i class="fas fa-times"></i> Cerrar
					</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Fin Modal -->
    
<?php require_once APP.'/vistas/include/footer.php'; ?>