<?php require_once APP.'/vistas/include/header.php';
$codigo = substr($_SESSION['codigoCaso'], 4);
$perfil = $obj->listar_casos('perfil', $codigo);
$usuario = $obj->info_usuario($perfil['codOpen'][0]);
$responsable = $usuario['nombres'][0].' '.$usuario['apellidos'][0];
?>
	<div class="container mt-5">
		<div class="row h-3em"></div>
		<div class="row">
			<div class="col-12 mb-4">
				<h1 class="display-4">
					<i class="fas fa-book-open"></i> Caso <strong><?php echo $_SESSION['codigoCaso']; ?></strong>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Tipo de caso</h5>
				<p><?php echo $perfil['caso'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Tipo de registro</h5>
				<p><?php echo $perfil['tipoRegistro'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Fecha del registro</h5>
				<p><?php echo $perfil['fechaRegistro'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Estado</h5>
				<p><?php echo $perfil['estado'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3  d-col-lg float-left">
				<h5 class="mt-2">Asunto</h5>
				<p><?php echo $perfil['asunto'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Nombre</h5>
				<p><?php echo $perfil['nombre'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Email</h5>
				<p><?php echo $perfil['email'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Oficina</h5>
				<p><?php echo $perfil['oficina'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 float-left bg-light-gray">
				<h5 class="mt-2">Mensaje</h5>
				<p><?php echo $perfil['mensaje'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 float-left">
				<h5 class="mt-2">Caso abierto por</h5>
				<p><?php echo $responsable; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12"></div>
		</div>
		<div class="row mt-2">
			<div class="col-12 col-lg-12 d-col-lg float-left">
				<a class="btn btn-dark" href="<?php echo URL.'casos-abiertos'; ?>">
					<i class="fas fa-backward"></i> Volver
				</a>
				<a class="btn btn-primary text-white" data-toggle="modal" data-target="#nueva-entrada">
					<i class="far fa-edit"></i> Nueva Entrada
				</a>
				<?php if ($obj->listar_entradas($codigo)): ?>
				<a class="btn bg-scarlet text-white float-right" data-toggle="modal" data-target="#cerrar-caso">
					<i class="far fa-folder"></i> Cerrar Caso
				</a>
				<?php endif ?>
			</div>
		</div>
		
<?php if ($obj->listar_entradas($codigo)): ?>

<?php $entradas = $obj->listar_entradas($codigo); $nEntradas = count($entradas['titulo']); ?>

<?php for ($i = 0; $i < $nEntradas; $i++): ?>

		<div class="row">
			<?php if ($i == 0): ?>
			<div class="col-12">
				<hr class="bg-dark">
			</div>
			<?php endif ?>
			<div class="col-12">
				<h3><?php echo $entradas['titulo'][$i]; ?></h3>
			</div>
		</div>
		
		<div class="row mt-1em">
			<div class="col-12 col-lg-12 d-col-lg float-left text-adjust">
				<h5>Descripción de la entrada</h5>
				<p class="text-justify"><?php echo $entradas['descEntrada'][$i]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-4 d-col-lg float-left">
				<h5>Documento</h5>
				<?php if ($entradas['rutaDoc'][$i] == ''): ?>
					<a class="btn bg-dark-green text-white" data-toggle="modal" data-target="<?php echo '#nuevo-documento'.$i; ?>">
						<i class="far fa-file-alt"></i> Subir Archivo
					</a>
				<?php else: ?>
					<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
						<button type="submit" name="descargar" class="btn btn-link" value="<?php echo $entradas['codEntrada'][$i]; ?>">
							Descargar "<?php echo $entradas['nombreDoc'][$i]; ?>"
						</button>
					</form>
				<?php endif ?>
			</div>
			<div class="col-12 col-lg-4 d-col-lg float-left">
				<h5>Responsable de la entrada</h5>
				<p><?php echo $entradas['responsable'][$i]; ?></p>
			</div>
			<div class="col-12 col-lg-4 d-col-lg float-left">
				<h5>Fecha de entrada</h5>
				<p><?php echo $entradas['fecha'][$i]; ?></p>
			</div>
		</div>

		<?php if ($entradas['rutaDoc'][$i] == ''): ?>

		<!-- Modal Agregar Documento -->

		<div class="modal fade" id="<?php echo 'nuevo-documento'.$i; ?>" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header bg-dark-green text-white">
						<h3 class="modal-title" id="modalForget"><i class="far fa-file-alt"></i> Nuevo documento</h3>
					</div>
					<div class="modal-body">
						<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
							<div class="form-group">
								<label for="nombre"><strong>Nombre:</strong></label>
								<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" autocomplete="off" required>
							</div>
							<div class="form-group">
							    <label for="file"><strong>Documento:</strong></label>
							    <input type="file" class="form-control-file" id="file" name="archivo" required>
							</div>
					</div>
					<div class="modal-footer">
							<button type="button" class="btn btn-dark" data-dismiss="modal">
								<i class="fas fa-times"></i> Cancelar
							</button>
							<input type="hidden" name="codRegistro" value="<?php echo $codigo; ?>">
							<button type="submit" class="btn bg-dark-green text-white" name="subir-doc" value="<?php echo $entradas['codEntrada'][$i]; ?>">
								<i class="fas fa-check"></i> Subir Documento
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- fin Modal -->

		<?php endif ?>


		<div class="row mt-2em">
			<div class="col-12 float-left">
				<a class="btn btn-secondary text-white" data-toggle="modal" data-target="<?php echo '#editar-entrada-'.$i; ?>">Editar Entrada</a>
				<a class="btn bg-scarlet text-white" href="<?php echo URL.'eliminar-entrada/'.$entradas['codEntrada'][$i]; ?>">Eliminar Entrada</a>
			</div>
			<div class="col-12">
				<hr class="bg-dark">
			</div>
		</div>

		<!-- Modal Editar Entrada -->

		<div class="modal fade" id="<?php echo 'editar-entrada-'.$i; ?>" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h3 class="modal-title" id="modalForget"><i class="far fa-edit"></i> Editar <?php echo $entradas['titulo'][$i]; ?></h3>
				</div>
				<div class="modal-body">
					<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8">
						<div class="form-group">
							<label for="titulo"><strong>Título de la entrada:</strong></label>
							<input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $entradas['titulo'][$i]; ?>" autocomplete="off" required>
						</div>
						<div class="form-group">
							<label for="descripcion"><strong>Descripción:</strong></label>
							<textarea class="form-control" id="descripcion" wrap="soft" name="descripcion" rows="5" maxlength="10000" required><?php echo $entradas['descEntrada'][$i]; ?></textarea>
						</div>
						<?php if ($entradas['rutaDoc'][$i] != ''): ?>
						<div class="form-group">
							<button type="submit" class="btn bg-scarlet text-white" name="eliminar-documento" value="<?php echo $entradas['codEntrada'][$i]; ?>">
								Eliminar <?php echo $entradas['nombreDoc'][$i]; ?>
							</button>
						</div>
						<?php endif ?>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-dark" data-dismiss="modal">
							<i class="fas fa-times"></i> Cancelar
						</button>
						<button type="submit" class="btn bg-dark-green text-white" name="editar-entrada" value="<?php echo $entradas['codEntrada'][$i]; ?>">
							<i class="fas fa-check"></i> Editar entrada
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
		
		<!-- fin Modal -->

<?php endfor ?>
		
<?php endif ?>
		
		<div class="row h-4em"></div>
	</div>

	<!-- Modal Nueva entrada -->

	<div class="modal fade" id="nueva-entrada" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h3 class="modal-title" id="modalForget"><i class="far fa-edit"></i> Escribir nueva entrada</h3>
				</div>
				<div class="modal-body">
					<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8">
						<div class="form-group">
							<label for="titulo"><strong>Título de la entrada:</strong></label>
							<input type="text" class="form-control" name="titulo" id="titulo" placeholder="Título" autocomplete="off" required>
						</div>
						<div class="form-group">
							<label for="descripcion"><strong>Descripción:</strong></label>
							<textarea class="form-control" id="descripcion" wrap="soft" name="descripcion" rows="5" placeholder="Cantidad de caracteres permitidos: 1,000" maxlength="10000" required></textarea>
						</div>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-dark" data-dismiss="modal">
							<i class="fas fa-times"></i> Cancelar
						</button>
						<button type="submit" class="btn bg-dark-green text-white" name="ingresar-entrada" value="<?php echo $codigo; ?>">
							<i class="fas fa-check"></i> Ingresar entrada
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Fin Modal -->

<?php if ($obj->listar_entradas($codigo)): ?>

	<!-- Modal Cerrar Caso -->

	<div class="modal fade" id="cerrar-caso" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
		<div class="modal-dialog" Cerrar Casoument">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h3 class="modal-title" id="modalForget">
						<i class="far fa-folder"></i> Cerrar caso <?php echo $_SESSION['codigoCaso']; ?>
					</h3>
				</div>
				<div class="modal-body">
					<h5>¿Estás seguro de cerrar el caso?</h5>
					<p>
						Al dar clic en <strong>Cerrar Caso</strong>, tu y todo el comité no podrán agregar y/o modificar ninguna entrada registrada.
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-dark" data-dismiss="modal">
						<i class="fas fa-times"></i> Cerrar
					</button>
					<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8">
						<button type="submit" class="btn bg-scarlet text-white" name="cerrar-caso">
							<i class="fas fa-door-open"></i> Cerrar Caso
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Fin Modal -->

<?php endif ?>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>