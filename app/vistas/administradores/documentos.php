<?php require_once APP.'/vistas/include/header.php'; ?>

	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4"><i class="fas fa-archive"></i> Documentos</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="alert alert-dark">
					<table class="w-100">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th width="30%">Nombre del Documento</th>
								<th width="20%">Fecha</th>
								<th width="30%">Responsable</th>
								<th width="10%">Acción</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="col-12" id="uniqueId">
				<?php $obj->listarDocumentos(); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a class="btn bg-dark-green text-white float-right" data-toggle="modal" data-target="#newDoc">
				  	<i class="fas fa-file-alt"></i> Nuevo Documento
				</a>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="newDoc" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h5 class="modal-title" id="modalForget"><i class="fas fa-file-upload"></i> Subir documento</h5>
				</div>
				<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="form-group">
							<label for="nombre"><strong>Nombre:</strong></label>
							<input type="text" class="form-control" name="nombreDoc" id="nombre" placeholder="Nombre" autocomplete="off" required>
						</div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="customFile" name="file" required>
							<label class="custom-file-label" for="customFile">Seleccionar Archivo.</label>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark" data-dismiss="modal">
							<i class="fas fa-times"></i> Cancelar
						</button>					
						<button type="submit" class="btn bg-dark-green text-white" name="docs-subir-doc">
							<i class="fas fa-check"></i> Subir Documento
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>