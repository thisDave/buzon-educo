<?php 
require_once APP.'/vistas/include/header.php';
//-----------------------------------------------------
$codigo = substr($_SESSION['codigoCaso'], 4);
//-----------------------------------------------------
$perfil = $obj->listar_casos('perfil', $codigo);
//-----------------------------------------------------
$usuario1 = $obj->info_usuario($perfil['codOpen'][0]);
$responsable1 = $usuario1['nombres'][0].' '.$usuario1['apellidos'][0];
//-----------------------------------------------------
$usuario2 = $obj->info_usuario($perfil['codClose'][0]);
$responsable2 = $usuario2['nombres'][0].' '.$usuario2['apellidos'][0];
//-----------------------------------------------------
$entradas = $obj->listar_entradas($codigo);
$nEntradas = count($entradas['titulo']);
//-----------------------------------------------------
if (isset($_SESSION['caso'])) { unset($_SESSION['caso']); }
//-----------------------------------------------------
?>
	<div class="container mt-5">
		<div class="row h-3em"></div>
		<div class="row">
			<div class="col-12 mb-4">
				<h1 class="display-4">
					<i class="far fa-address-book"></i> Caso <strong><?php echo $_SESSION['codigoCaso']; ?></strong><strong class="display-5"> (Cerrado)</strong>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Tipo de caso</h5>
				<p><?php echo $perfil['caso'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Tipo de registro</h5>
				<p><?php echo $perfil['tipoRegistro'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Fecha del registro</h5>
				<p><?php echo $perfil['fechaRegistro'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Estado</h5>
				<p><?php echo $perfil['estado'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3  d-col-lg float-left">
				<h5 class="mt-2">Asunto</h5>
				<p><?php echo $perfil['asunto'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Nombre</h5>
				<p><?php echo $perfil['nombre'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Email</h5>
				<p><?php echo $perfil['email'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Oficina</h5>
				<p><?php echo $perfil['oficina'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 float-left bg-light-gray">
				<h5 class="mt-2">Mensaje</h5>
				<p><?php echo $perfil['mensaje'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Caso abierto por</h5>
				<p><?php echo $responsable1; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Caso cerrado por</h5>
				<p><?php echo $responsable2; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Fecha de apertura</h5>
				<p><?php echo $perfil['fechaApertura'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Fecha de cierre</h5>
				<p><?php echo $perfil['fechaCierre'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-4 d-col-lg float-left"></div>
		</div>
		<div class="row">
			<div class="col-12"></div>
		</div>

<?php for ($i = 0; $i < $nEntradas; $i++): ?>

		<div class="row">
			<?php if ($i == 0): ?>
			<div class="col-12">
				<hr class="bg-dark">
			</div>
			<?php endif ?>
			<div class="col-12">
				<h3><?php echo $entradas['titulo'][$i]; ?></h3>
			</div>
		</div>
		
		<div class="row mt-1em">
			<div class="col-12 col-lg-12 d-col-lg float-left text-adjust">
				<h5>Descripción de la entrada</h5>
				<p class="text-justify"><?php echo $entradas['descEntrada'][$i]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 d-col-lg float-left">
				<h5>Documento</h5>
				<?php if ($entradas['rutaDoc'][$i] != ''): ?>
					<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
						<button type="submit" name="descargar" class="btn btn-link" value="<?php echo $entradas['codEntrada'][$i]; ?>">
							Descargar "<?php echo $entradas['nombreDoc'][$i]; ?>"
						</button>
					</form>
				<?php else: ?>
					<p>Empty</p>
				<?php endif ?>
			</div>
			<div class="col-12 col-lg-6 d-col-lg float-left">
				<h5>Fecha de entrada</h5>
				<p><?php echo $entradas['fecha'][$i]; ?></p>
			</div>
		</div>

		<div class="row">
			<div class="col-12 float-left">
				<?php $hEntrada = $obj->historial_entrada($entradas['codEntrada'][$i]); ?>
				<h5>Responsables de la entrada</h5>
				<?php 
				$personas = array_unique($hEntrada['nombre']);
				$n = count($personas);
				?>
				<p>
				<?php
				$contador = 1;
				foreach ($personas as $key => $value)
				{
					if ($contador == $n)
						echo $value;
					else
						echo $value.', ';
					$contador++;
				}
				?>
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<hr class="bg-dark">
			</div>
		</div>

<?php endfor ?>

		<div class="row mt-2">
			<div class="col-12 col-lg-12 d-col-lg float-left">
				<a class="btn btn-dark" href="<?php echo URL.'casos-cerrados'; ?>">
					<i class="fas fa-backward"></i> Volver
				</a>
				<a class="btn bg-scarlet text-white float-right" href="<?php echo URL.'reportes-pdf/caso-cerrado'; ?>">
					<i class="far fa-file-pdf"></i> Imprimir caso
				</a>
			</div>
		</div>
		
		<div class="row h-4em"></div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>