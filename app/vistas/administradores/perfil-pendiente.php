<?php require_once APP.'/vistas/include/header.php';
$codigo = substr($_SESSION['codigoCaso'], 4);
$perfil = $obj->listar_casos('perfil', $codigo);
?>
	<div class="container mt-5">
		<div class="row h-3em"></div>
		<div class="row">
			<div class="col-12 mb-4">
				<h1 class="display-4">
					<i class="fas fa-suitcase"></i> Caso <strong><?php echo $_SESSION['codigoCaso']; ?></strong>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Tipo de caso</h5>
				<p><?php echo $perfil['caso'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Tipo de registro</h5>
				<p><?php echo $perfil['tipoRegistro'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Fecha del registro</h5>
				<p><?php echo $perfil['fechaRegistro'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Estado</h5>
				<p><?php echo $perfil['estado'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-3  d-col-lg float-left">
				<h5 class="mt-2">Asunto</h5>
				<p><?php echo $perfil['asunto'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Nombre</h5>
				<p><?php echo $perfil['nombre'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Email</h5>
				<p><?php echo $perfil['email'][0]; ?></p>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-2">Oficina</h5>
				<p><?php echo $perfil['oficina'][0]; ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-12  d-col-lg float-left bg-light-gray">
				<h5 class="mt-2">Mensaje</h5>
				<p><?php echo $perfil['mensaje'][0]; ?></p>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-12 col-lg-12  d-col-lg float-left">
				<a class="btn btn-dark" href="<?php echo URL.'casos-pendientes'; ?>">
					<i class="fas fa-backward"></i> Volver
				</a>
				<a class="btn btn-primary text-white" data-toggle="modal" data-target="#abrir-caso">
					<i class="far fa-folder-open"></i> Abrir caso
				</a>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>

	<!-- Modal -->

	<div class="modal fade" id="abrir-caso" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h3 class="modal-title" id="modalForget">
						<i class="far fa-folder-open"></i> Iniciar caso <?php echo $_SESSION['codigoCaso']; ?>
					</h3>
				</div>
				<div class="modal-body">
					<h5>¿Estás seguro de abrir el caso?</h5>
					<p>
						Al abrir el caso, tu y todo el comité serán responsables de llevar el proceso hasta su resolución dejando constancia en el módulo de <i>Casos Abiertos</i>, y de ser necesario adjuntar documentos. Posteriormente podrás observar un reporte completo del caso, este aparecerá al finalizar el proceso.
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-dark" data-dismiss="modal">
						<i class="fas fa-times"></i> Cerrar
					</button>
					<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8">
						<button type="submit" class="btn bg-dark-green text-white" name="abrir-caso" value="<?php echo $codigo; ?>">
							<i class="fas fa-door-open"></i> Abrir Caso
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>