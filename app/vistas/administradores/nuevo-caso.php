<?php require_once APP.'/vistas/include/header.php'; ?>

	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4"><i class="fas fa-paste"></i> Agregar un caso</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form class="mt-3" action="<?php echo URL; ?>" method="post" accept-charset="utf8">
					<div class="form-row">
						<div class="form-group col-12">
							<label for="ol">Oficina local:</label>
							<select name="ol" id="ol" class="form-control" required>
								<option value="">Selecciona tu oficina local</option>
								<option value="2">Región Centro</option>
								<option value="1">Región Occidente</option>
								<option value="3">Región Oriente</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-7">
							<label for="asunto">Asunto:</label>
							<input type="text" class="form-control" id="asunto" name="asunto" aria-describedby="asuntoHelp" placeholder="Escribe el asunto de tu mensaje" autocomplete="off" required>
							<small id="asuntoHelp" class="form-text text-muted">Escribe brevemente el asunto de tu queja, sugerencia o reconocimiento.</small>
						</div>
						<div class="form-group col-5">
							<label for="tipo">Tipo de caso:</label>
							<select name="tipo" id="tipo" class="form-control" required>
								<option value="">Selecciona tu oficina local</option>
								<option value="1">Queja</option>
								<option value="2">Sugerencia</option>
								<option value="3">Felicitaciones</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12">
							<label for="descripcion">Describe brevemente el caso:</label>
							<textarea class="form-control" id="descripcion" wrap="soft" name="mensaje" rows="5" placeholder="Cantidad de caracteres permitidos: 1,000" maxlength="10000" required></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12">
							<small class="text-secondary">
								La información registrada en este formulario será reflejada con la información de tu usuario.
							</small>
						</div>
					</div>
					<button type="submit" name="registroCasoLogin" class="btn bg-dark-green text-white">
						<i class="fas fa-edit"></i> Enviar Caso
					</button>
				</form>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>