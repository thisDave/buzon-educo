<?php require_once APP.'/vistas/include/header.php'; $datos = $obj->charts(); ?>

	<!-- Begin Chart Js -->

	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/dist/Chart.js"></script>

	<?php

	$datos = $obj->charts();

	switch ($_SESSION['chart'])
	{
		case 'tipo-barra':
			?>
			<script>
				$(document).ready(function(){

					var datos = {
						labels : ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
						datasets : [{
							label : "Personales",
							backgroundColor : "rgba(17,125,90,0.5)",
							data : [<?php echo $datos['allPersonal']['Enero'][0].','.$datos['allPersonal']['Febrero'][0].','.$datos['allPersonal']['Marzo'][0].','.$datos['allPersonal']['Abril'][0].','.$datos['allPersonal']['Mayo'][0].','.$datos['allPersonal']['Junio'][0].','.$datos['allPersonal']['Julio'][0].','.$datos['allPersonal']['Agosto'][0].','.$datos['allPersonal']['Septiembre'][0].','.$datos['allPersonal']['Octubre'][0].','.$datos['allPersonal']['Noviembre'][0].','.$datos['allPersonal']['Diciembre'][0];
							?>]
						},
						{
							label : "Anonimos",
							backgroundColor : "rgba(35,148,140,0.5)",
							data : [<?php echo $datos['allAnonimous']['Enero'][0].','.$datos['allAnonimous']['Febrero'][0].','.$datos['allAnonimous']['Marzo'][0].','.$datos['allAnonimous']['Abril'][0].','.$datos['allAnonimous']['Mayo'][0].','.$datos['allAnonimous']['Junio'][0].','.$datos['allAnonimous']['Julio'][0].','.$datos['allAnonimous']['Agosto'][0].','.$datos['allAnonimous']['Septiembre'][0].','.$datos['allAnonimous']['Octubre'][0].','.$datos['allAnonimous']['Noviembre'][0].','.$datos['allAnonimous']['Diciembre'][0];
							?>]
						}
						]
					};

					var canvas = document.getElementById('tipo-barra').getContext('2d');
					
					window.bar = new Chart(canvas, {
						type : "bar",
						data : datos,
						options : {
							elements : {
								rectangle : {
									borderWidth : 1,
									borderColor : "rgb(132,175,156)",
									borderSkipped : 'bottom'
								}
							},
							responsive : true,
							title : {
								display : true
							}
						}
					});
				});
			</script>
			<?php
		break;

		case 'tipo-pastel':
		?>
			<script>
				$(document).ready(function(){
					var datos = {
						type: "pie",
						data : {
							datasets :[{
								data : [<?php echo $datos['Personales'][0].','.$datos['Anonimos'][0]; ?>],
								backgroundColor: ["rgba(17,125,90,0.5)", "rgba(35,148,140,0.5)"],
							}],
							labels : ["Personales", "Anonimos"]
						},
						options : {
							responsive : true,
							title : {
								display : true
							}
						}
					};

					var canvas = document.getElementById('tipo-pastel').getContext('2d');
					window.pie = new Chart(canvas, datos);
				});
			</script>
		<?php	
		break;

		case 'categoria-pastel':
			?>
			<script>
				$(document).ready(function(){
					var datos = {
						type: "pie",
						data : {
							datasets :[{
								data : [<?php echo $datos['quejas'][0].','.$datos['sugerencias'][0].','.$datos['reconocimientos'][0]; ?>],
								backgroundColor: [ "rgba(207,66,65,0.6)", "rgba(242,178,28,0.6)", "rgba(17,125,90,0.6)"],
							}],
							labels : ["quejas", "sugerencias", "reconocimientos"]
						},
						options : {
							responsive : true,
							title : {
								display : true
							}
						}
					};

					var canvas = document.getElementById('categoria-pastel').getContext('2d');
					window.pie = new Chart(canvas, datos);
				});
			</script>
			<?php
		break;

		case 'categoria-barra':
			?>
			<script>
				$(document).ready(function(){
				
					var datos = {
						labels : ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
						datasets : [{
							label : "Quejas",
							backgroundColor : "rgba(207,66,65,0.5)",
							data : [<?php echo $datos['allQ']['Enero'][0].','.$datos['allQ']['Febrero'][0].','.$datos['allQ']['Marzo'][0].','.$datos['allQ']['Abril'][0].','.$datos['allQ']['Mayo'][0].','.$datos['allQ']['Junio'][0].','.$datos['allQ']['Julio'][0].','.$datos['allQ']['Agosto'][0].','.$datos['allQ']['Septiembre'][0].','.$datos['allQ']['Octubre'][0].','.$datos['allQ']['Noviembre'][0].','.$datos['allQ']['Diciembre'][0];
							?>]
						},
						{
							label : "Sugerencias",
							backgroundColor : "rgba(242,178,28,0.5)",
							data : [<?php echo $datos['allS']['Enero'][0].','.$datos['allS']['Febrero'][0].','.$datos['allS']['Marzo'][0].','.$datos['allS']['Abril'][0].','.$datos['allS']['Mayo'][0].','.$datos['allS']['Junio'][0].','.$datos['allS']['Julio'][0].','.$datos['allS']['Agosto'][0].','.$datos['allS']['Septiembre'][0].','.$datos['allS']['Octubre'][0].','.$datos['allS']['Noviembre'][0].','.$datos['allS']['Diciembre'][0];
							?>]
						},
						{
							label : "Reconocimientos",
							backgroundColor : "rgba(17,125,90,0.5)",
							data : [<?php echo $datos['allR']['Enero'][0].','.$datos['allR']['Febrero'][0].','.$datos['allR']['Marzo'][0].','.$datos['allR']['Abril'][0].','.$datos['allR']['Mayo'][0].','.$datos['allR']['Junio'][0].','.$datos['allR']['Julio'][0].','.$datos['allR']['Agosto'][0].','.$datos['allR']['Septiembre'][0].','.$datos['allR']['Octubre'][0].','.$datos['allR']['Noviembre'][0].','.$datos['allR']['Diciembre'][0];
							?>]
						}
						]
					};

					var canvas = document.getElementById('categoria-barra').getContext('2d');
					
					window.bar = new Chart(canvas, {
						type : "bar",
						data : datos,
						options : {
							elements : {
								rectangle : {
									borderWidth : 1,
									borderColor : "rgb(132,175,156)",
									borderSkipped : 'bottom'
								}
							},
							responsive : true,
							title : {
								display : true
							}
						}
					});
				});
			</script>
			<?php
		break;

		case 'estado-barra':
			?>
			<script>
				$(document).ready(function(){
				
					var datos = {
						labels : ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
						datasets : [{
							label : "Pendientes",
							backgroundColor : "rgba(207,66,65,0.5)",
							data : [<?php echo $datos['allP']['Enero'][0].','.$datos['allP']['Febrero'][0].','.$datos['allP']['Marzo'][0].','.$datos['allP']['Abril'][0].','.$datos['allP']['Mayo'][0].','.$datos['allP']['Junio'][0].','.$datos['allP']['Julio'][0].','.$datos['allP']['Agosto'][0].','.$datos['allP']['Septiembre'][0].','.$datos['allP']['Octubre'][0].','.$datos['allP']['Noviembre'][0].','.$datos['allP']['Diciembre'][0];
							?>]
						},
						{
							label : "Abiertos",
							backgroundColor : "rgba(242,178,28,0.5)",
							data : [<?php echo $datos['allA']['Enero'][0].','.$datos['allA']['Febrero'][0].','.$datos['allA']['Marzo'][0].','.$datos['allA']['Abril'][0].','.$datos['allA']['Mayo'][0].','.$datos['allA']['Junio'][0].','.$datos['allA']['Julio'][0].','.$datos['allA']['Agosto'][0].','.$datos['allA']['Septiembre'][0].','.$datos['allA']['Octubre'][0].','.$datos['allA']['Noviembre'][0].','.$datos['allA']['Diciembre'][0];
							?>]
						},
						{
							label : "Cerrados",
							backgroundColor : "rgba(17,125,90,0.5)",
							data : [<?php echo $datos['allC']['Enero'][0].','.$datos['allC']['Febrero'][0].','.$datos['allC']['Marzo'][0].','.$datos['allC']['Abril'][0].','.$datos['allC']['Mayo'][0].','.$datos['allC']['Junio'][0].','.$datos['allC']['Julio'][0].','.$datos['allC']['Agosto'][0].','.$datos['allC']['Septiembre'][0].','.$datos['allC']['Octubre'][0].','.$datos['allC']['Noviembre'][0].','.$datos['allC']['Diciembre'][0];
							?>]
						}
						]
					};

					var canvas = document.getElementById('estado-barra').getContext('2d');
					
					window.bar = new Chart(canvas, {
						type : "bar",
						data : datos,
						options : {
							elements : {
								rectangle : {
									borderWidth : 1,
									borderColor : "rgb(132,175,156)",
									borderSkipped : 'bottom'
								}
							},
							responsive : true,
							title : {
								display : true
							}
						}
					});
				});
			</script>
			<?php
		break;

		case 'estado-pastel':
			?>
			<script>
				$(document).ready(function(){
					var datos = {
						type: "pie",
						data : {
							datasets :[{
								data : [<?php echo $datos['Pendientes'][0].','.$datos['Abiertos'][0].','.$datos['Cerrados'][0]; ?>],
								backgroundColor: [ "rgba(207,66,65,0.6)", "rgba(242,178,28,0.6)", "rgba(17,125,90,0.6)"],
							}],
							labels : ["Pendientes", "Abiertos", "Cerrados"]
						},
						options : {
							responsive : true,
							title : {
								display : true
							}
						}
					};

					var canvas = document.getElementById('estado-pastel').getContext('2d');
					window.pie = new Chart(canvas, datos);
				});
			</script>
			<?php
		break;
	}
	?>

	<!-- End Chart Js -->
	
	<div class="container mt-5">
		<div class="row h-2em"></div>

		<?php 
		switch ($_SESSION['chart'])
		{
			case 'tipo-barra': ?>

		<div class="row">
			<div class="col-12">
				<h4 class="display-5 text-center">Registro general separados por tipo</h4>
				<canvas id="tipo-barra" height="100"></canvas>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<a class="btn bg-dark-green text-white" href="<?= URL ?>">
					<i class="fas fa-dot-circle"></i> Por Tipo
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/categoria-barra">
					<i class="fas fa-project-diagram"></i> Por categoría
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/estado-barra">
					<i class="fas fa-thermometer-half"></i> Por Estado
				</a>
			</div>
			<div class="col-6">
				<a class="btn btn-warning float-right" href="<?= URL ?>casos-charts/tipo-pastel"><i class="fas fa-birthday-cake"></i> Tipo Pastel</a>
			</div>
		</div>

		<?php	break;

			case 'tipo-pastel': ?>

		<div class="row">
			<div class="col-12 col-lg-9 d-col-lg float-left">
				<canvas id="tipo-pastel" height="155"></canvas>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<p class="text-justify mt-5">
					La presente gráfica presenta los datos en tiempo real de los casos registrados separados por tipo de atención.
				</p>
				<p class="text-justify">
					Actualmente se presentan los siguientes datos:
					<ul>
						<li>Personales: <?php echo $datos['Personales'][0]; ?></li>
						<li>Anonimos: <?php echo $datos['Anonimos'][0]; ?></li>						
					</ul>
				</p>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<a class="btn bg-dark-green text-white" href="<?= URL ?>">
					<i class="fas fa-dot-circle"></i> Por Tipo
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/categoria-barra">
					<i class="fas fa-project-diagram"></i> Por categoría
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/estado-barra">
					<i class="fas fa-thermometer-half"></i> Por Estado
				</a>
			</div>
			<div class="col-6">
				<a class="btn btn-warning float-right" href="<?= URL ?>casos-charts/tipo-barra"><i class="fas fa-chart-bar"></i> Tipo Barra</a>
			</div>
		</div>

		<?php	break;

			case 'categoria-pastel': ?>

		<div class="row">
			<div class="col-12 col-lg-8 d-col-lg float-left">
				<canvas id="categoria-pastel" height="155"></canvas>
			</div>
			<div class="col-12 col-lg-4 d-col-lg float-left">
				<h5 class="mt-4">Casos Atendidos en el año</h5>
				<p>
					<table>
						<thead>
							<tr>
								<th width="100" height="20"></th>
								<th width="50" style="background-color: rgba(207,66,65,0.5);"></th>
								<th width="50" style="background-color: rgba(242,178,28,0.5);"></th>
								<th width="50" style="background-color: rgba(17,125,90,0.5);"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Enero</td>
								<td class="text-center"><?php echo $datos['allQ']['Enero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Enero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Enero'][0]; ?></td>
							</tr>
							<tr>
								<td>Febrero</td>
								<td class="text-center"><?php echo $datos['allQ']['Febrero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Febrero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Febrero'][0]; ?></td>
							</tr>
							<tr>
								<td>Marzo</td>
								<td class="text-center"><?php echo $datos['allQ']['Marzo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Marzo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Marzo'][0]; ?></td>
							</tr>
							<tr>
								<td>Abril</td>
								<td class="text-center"><?php echo $datos['allQ']['Abril'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Abril'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Abril'][0]; ?></td>
							</tr>
							<tr>
								<td>Mayo</td>
								<td class="text-center"><?php echo $datos['allQ']['Mayo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Mayo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Mayo'][0]; ?></td>
							</tr>
							<tr>
								<td>Junio</td>
								<td class="text-center"><?php echo $datos['allQ']['Junio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Junio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Junio'][0]; ?></td>
							</tr>
							<tr>
								<td>Julio</td>
								<td class="text-center"><?php echo $datos['allQ']['Julio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Julio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Julio'][0]; ?></td>
							</tr>
							<tr>
								<td>Agosto</td>
								<td class="text-center"><?php echo $datos['allQ']['Agosto'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Agosto'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Agosto'][0]; ?></td>
							</tr>
							<tr>
								<td>Septiembre</td>
								<td class="text-center"><?php echo $datos['allQ']['Septiembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Septiembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Septiembre'][0]; ?></td>
							</tr>
							<tr>
								<td>Octubre</td>
								<td class="text-center"><?php echo $datos['allQ']['Octubre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Octubre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Octubre'][0]; ?></td>
							</tr>
							<tr>
								<td>Noviembre</td>
								<td class="text-center"><?php echo $datos['allQ']['Noviembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Noviembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Noviembre'][0]; ?></td>
							</tr>
							<tr>
								<td>Diciembre</td>
								<td class="text-center"><?php echo $datos['allQ']['Diciembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allS']['Diciembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allR']['Diciembre'][0]; ?></td>
							</tr>
							<tr class="bg-light-gray">
								<td><strong>Total</strong></td>
								<td class="text-center"><strong><?php echo $datos['quejas'][0]; ?></strong></td>
								<td class="text-center"><strong><?php echo $datos['sugerencias'][0]; ?></strong></td>
								<td class="text-center"><strong><?php echo $datos['reconocimientos'][0]; ?></strong></td>
							</tr>
						</tbody>
					</table>
				</p>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/tipo-barra">
					<i class="fas fa-dot-circle"></i> Por Tipo
				</a>
				<a class="btn bg-dark-green text-white" href="<?= URL ?>">
					<i class="fas fa-project-diagram"></i> Por categoría
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/estado-barra">
					<i class="fas fa-thermometer-half"></i> Por Estado
				</a>
			</div>
			<div class="col-6">
				<a class="btn btn-warning float-right" href="<?= URL ?>casos-charts/categoria-barra"><i class="fas fa-chart-bar"></i> Tipo Barra</a>
			</div>
		</div>

		<?php	break;

			case 'categoria-barra': ?>

		<div class="row">
			<div class="col-12">
				<h4 class="display-5 text-center">Registro general separados por categoría</h4>
				<canvas id="categoria-barra" height="100"></canvas>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/tipo-barra">
					<i class="fas fa-dot-circle"></i> Por Tipo
				</a>
				<a class="btn bg-dark-green text-white" href="<?= URL ?>">
					<i class="fas fa-project-diagram"></i> Por categoría
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/estado-barra">
					<i class="fas fa-thermometer-half"></i> Por Estado
				</a>
			</div>
			<div class="col-6">
				<a class="btn btn-warning float-right" href="<?= URL ?>casos-charts/categoria-pastel"><i class="fas fa-birthday-cake"></i> Tipo Pastel</a>
			</div>
		</div>

		<?php	break;

			case 'estado-barra': ?>

		<div class="row">
			<div class="col-12">
				<h4 class="display-5 text-center">Registro general separados por Estado</h4>
				<canvas id="estado-barra" height="100"></canvas>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/tipo-barra">
					<i class="fas fa-dot-circle"></i> Por Tipo
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/categoria-barra">
					<i class="fas fa-project-diagram"></i> Por categoría
				</a>
				<a class="btn bg-dark-green text-white" href="<?= URL ?>">
					<i class="fas fa-thermometer-half"></i> Por Estado
				</a>
			</div>
			<div class="col-6">
				<a class="btn btn-warning float-right" href="<?= URL ?>casos-charts/estado-pastel"><i class="fas fa-birthday-cake"></i> Tipo Pastel</a>
			</div>
		</div>

		<?php	break;

			case 'estado-pastel': ?>

		<div class="row">
			<div class="col-12 col-lg-9 d-col-lg float-left">
				<canvas id="estado-pastel" height="155"></canvas>
			</div>
			<div class="col-12 col-lg-3 d-col-lg float-left">
				<h5 class="mt-4">Casos Atendidos en el año</h5>
				<p>
					<table>
						<thead>
							<tr>
								<th width="100" height="20"></th>
								<th width="50" style="background-color: rgba(207,66,65,0.5);"></th>
								<th width="50" style="background-color: rgba(242,178,28,0.5);"></th>
								<th width="50" style="background-color: rgba(17,125,90,0.5);"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Enero</td>
								<td class="text-center"><?php echo $datos['allP']['Enero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Enero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Enero'][0]; ?></td>
							</tr>
							<tr>
								<td>Febrero</td>
								<td class="text-center"><?php echo $datos['allP']['Febrero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Febrero'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Febrero'][0]; ?></td>
							</tr>
							<tr>
								<td>Marzo</td>
								<td class="text-center"><?php echo $datos['allP']['Marzo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Marzo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Marzo'][0]; ?></td>
							</tr>
							<tr>
								<td>Abril</td>
								<td class="text-center"><?php echo $datos['allP']['Abril'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Abril'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Abril'][0]; ?></td>
							</tr>
							<tr>
								<td>Mayo</td>
								<td class="text-center"><?php echo $datos['allP']['Mayo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Mayo'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Mayo'][0]; ?></td>
							</tr>
							<tr>
								<td>Junio</td>
								<td class="text-center"><?php echo $datos['allP']['Junio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Junio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Junio'][0]; ?></td>
							</tr>
							<tr>
								<td>Julio</td>
								<td class="text-center"><?php echo $datos['allP']['Julio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Julio'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Julio'][0]; ?></td>
							</tr>
							<tr>
								<td>Agosto</td>
								<td class="text-center"><?php echo $datos['allP']['Agosto'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Agosto'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Agosto'][0]; ?></td>
							</tr>
							<tr>
								<td>Septiembre</td>
								<td class="text-center"><?php echo $datos['allP']['Septiembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Septiembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Septiembre'][0]; ?></td>
							</tr>
							<tr>
								<td>Octubre</td>
								<td class="text-center"><?php echo $datos['allP']['Octubre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Octubre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Octubre'][0]; ?></td>
							</tr>
							<tr>
								<td>Noviembre</td>
								<td class="text-center"><?php echo $datos['allP']['Noviembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Noviembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Noviembre'][0]; ?></td>
							</tr>
							<tr>
								<td>Diciembre</td>
								<td class="text-center"><?php echo $datos['allP']['Diciembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allA']['Diciembre'][0]; ?></td>
								<td class="text-center"><?php echo $datos['allC']['Diciembre'][0]; ?></td>
							</tr>
							<tr class="bg-light-gray">
								<td><strong>Total</strong></td>
								<td class="text-center"><strong><?php echo $datos['Pendientes'][0]; ?></strong></td>
								<td class="text-center"><strong><?php echo $datos['Abiertos'][0]; ?></strong></td>
								<td class="text-center"><strong><?php echo $datos['Cerrados'][0]; ?></strong></td>
							</tr>
						</tbody>
					</table>
				</p>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/tipo-barra">
					<i class="fas fa-dot-circle"></i> Por Tipo
				</a>
				<a class="btn btn-primary" href="<?= URL ?>casos-charts/categoria-barra">
					<i class="fas fa-project-diagram"></i> Por categoría
				</a>
				<a class="btn bg-dark-green text-white" href="<?= URL ?>">
					<i class="fas fa-thermometer-half"></i> Por Estado
				</a>
			</div>
			<div class="col-6">
				<a class="btn btn-warning float-right" href="<?= URL ?>casos-charts/estado-barra"><i class="fas fa-chart-bar"></i> Tipo Barra</a>
			</div>
		</div>

		<?php	break;

		}
		?>

	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>