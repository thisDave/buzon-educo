<?php require_once APP.'/vistas/include/header.php'; 

if ($obj->listar_casos('todo', 4))
{
	if (isset($_SESSION['clave-busqueda']))
		$datos = $obj->listar_casos('buscar', 4);
	else
		$datos = $obj->listar_casos('todo', 4);

	if (empty($datos))
	{
		$nCasos = 0;
	}
	else
	{
		$nCasos = count($datos['codigo']);
	}
}
else
{
	$nCasos = 0;
}

?>
	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12 col-lg-8 d-col-lg">
				<h1 class="display-4"><i class="fas fa-folder-open"></i> Casos Abiertos</h1>
			</div>
			<div class="col-12 col-lg-4 d-col-lg">
				<form class="form-inline mt-3" action="<?php echo URL; ?>" method="post">
					<div class="form-group mx-sm-3 mb-2">
						<?php if (isset($_SESSION['clave-busqueda'])): ?>
						<input type="text" class="form-control" name="clave" autocomplete="off" value="<?= $_SESSION['clave-busqueda'] ?>">
						<?php else: ?>
						<input type="text" class="form-control" name="clave" autocomplete="off" placeholder="Buscar...">
						<?php endif ?>
					</div>
					<button type="submit" name="buscar-caso" class="btn btn-outline-secondary mb-2">
						<i class="fas fa-search"></i> Buscar
					</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="alert alert-dark">
					<table class="w-100">
						<thead>
							<tr>
								<th width="10%">Código</th>
								<th width="30%">Asunto</th>
								<th width="15%">Fecha</th>
								<th width="20%">Caso</th>
								<th width="10%">Tipo</th>
								<th width="15%">Acción</th>
							</tr>
						</thead>
					</table>
				</div>
				<div id="uniqueId">
					<?php for ($i = 0; $i < $nCasos; $i++): ?>
						<div class="alert alert-light text-dark">
							<table class="w-100">
								<tbody>
									<tr>
										<th width="10%"><?php echo $datos['codigo'][$i]; ?></th>
										<td width="30%"><?php echo $datos['asunto'][$i]; ?></td>
										<td width="15%"><?php echo $datos['fechaRegistro'][$i]; ?></td>
										<td width="20%"><?php echo $datos['caso'][$i]; ?></td>
										<td width="10%"><?php echo $datos['tipoRegistro'][$i]; ?></td>
										<td width="15%"><a href="<?php echo URL.'perfil-abierto/'.$datos['codigo'][$i]; ?>">Dar seguimiento</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php endfor ?>
				</div>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>