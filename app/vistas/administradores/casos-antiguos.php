<?php require_once APP.'/vistas/include/header.php'; 

if ($obj->listar_casos('antiguos', null))
{
	$datos = $obj->listar_casos('antiguos', null);
	$nCasos = count($datos['codigo']);
}
else
{
	$nCasos = 0;
}

?>
	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4">Casos Antiguos</h1>
				<div class="alert alert-dark">
					<table class="w-100">
						<thead>
							<tr>
								<th width="8%">Código</th>
								<th width="15%">Tipo</th>
								<th width="25%">Nombre</th>
								<th width="23%">Email</th>
								<th width="12%">Fecha</th>
								<th width="17%">Oficina</th>
							</tr>
						</thead>
					</table>
				</div>
				<div id="uniqueId">
					<?php for ($i = 0; $i < $nCasos; $i++): ?>
						<div class="alert alert-light text-dark">
							<table class="w-100">
							  <tbody>
							    <tr>
									<th width="8%"><?php echo $datos['codigo'][$i]; ?></th>
									<td width="15%"><?php echo $datos['caso'][$i]; ?></td>
									<td width="25%"><?php echo $datos['nombre'][$i]; ?></td>
									<td width="23%"><?php echo $datos['email'][$i]; ?></td>
									<td width="12%"><?php echo $datos['fechaRegistro'][$i]; ?></td>
									<td width="17%"><?php echo $datos['oficina'][$i]; ?></td>
								</tr>
							  </tbody>
							</table>
						</div>
					<?php endfor ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a class="btn btn-dark" href="<?php echo URL.'casos-pendientes'; ?>">
					<i class="fas fa-backward"></i> Volver
				</a>
				<a class="btn btn-success float-right" href="<?php echo URL.'reportes-xls/reporte-antiguos'; ?>">
					<i class="far fa-file-excel"></i> Descargar .xlsx
				</a>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>