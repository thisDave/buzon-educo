<?php require_once APP.'/vistas/include/header.php'; ?>

	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4">Mensajes</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="alert alert-dark">
					<table class="w-100">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th width="30%">Remitente</th>
								<th width="30%">Asunto</th>
								<th width="20%">Fecha de recepción</th>
								<th width="10%">Acción</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="col-12" id="uniqueId">
				
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a class="btn bg-dark-green text-white float-right" href="<?= URL ?>newMnsj">
				  	<i class="far fa-envelope"></i> Nuevo Mensaje
				</a>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>

	<?php if (isset($_SESSION['mensaje'])): ?>
	<!-- Modal -->
	<div class="modal fade" id="newMensaje" tabindex="-1" role="dialog" aria-labelledby="modalForget" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h5 class="modal-title" id="modalForget"><i class="far fa-envelope"></i> Nuevo Mensaje</h5>
				</div>
				<form action="<?php echo URL; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<div class="modal-footer">
						<a class="btn btn-dark" href="<?= URL ?>deleteMnsj">
							<i class="fas fa-times"></i> Cancelar
						</a>
						<button type="submit" class="btn bg-dark-green text-white" name="docs-subir-doc">
							<i class="fas fa-check"></i> Subir Documento
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php endif ?>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>