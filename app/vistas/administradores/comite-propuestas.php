<?php require_once APP.'/vistas/include/header.php'; ?>
	<div class="container mt-5">
		<div class="row h-3em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4"><i class="fas fa-flag"></i> Propuestas del comité</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<table class="table">
					<thead>
						<tr>
							<th>N°</th>
							<th>Título</th>
							<th>Responsable</th>
							<th>Fecha</th>
							<th>Estado</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if ($obj->propuestas())
						{
						 	$propuestas = $obj->propuestas();

							$n = count($propuestas['codPropuesta']);

							$no = 1;
							for ($i = 0; $i < $n; $i++)
							{
								$responsable = $obj->info_usuario($propuestas['codUsuario'][$i]);

								echo '<tr>
										<td>'.$no.'</td>
										<td>'.$propuestas['titulo'][$i].'</td>
										<td>'.$responsable['nombres'][0].' '.$responsable['apellidos'][0].'</td>
										<td>'.$propuestas['fecha'][$i].'</td>
										<td>Pendiente</td>
										<td><a class="btn btn-link" href="'.URL.'comite-propuestas-perfil/'.$propuestas['codPropuesta'][$i].'">Ver propuesta</a></td>
									</tr>';
								$no++;
							}
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-12">
				<a class="btn btn-dark text-white" href="<?php echo URL.'comite-integrantes'; ?>">
					<i class="fas fa-backward"></i> Volver
				</a>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>
<?php require_once APP.'/vistas/include/footer.php'; ?>