<?php require_once APP.'/vistas/include/header.php'; 

if ($obj->info_usuario($_SESSION['codUsuario']))
{
	$usuario = $obj->info_usuario($_SESSION['codUsuario']);
	$n = count($usuario['nombres']);

	for ($i = 0; $i < $n; $i++)
	{
		$nombres = $usuario['nombres'][$i];
		$apellidos = $usuario['apellidos'][$i];
	}
}

?>
	<div class="container mt-5">
		<div class="row h-3em"></div>
		<div class="row">
			<div class="col-12">
				<h1 class="display-4">Hola <?php echo $nombres.' '.$apellidos; ?></h1>
				<p class="text-justify">
					Te damos la bienvenida al nuevo sistema de control del comité de quejas y sugerencias, a continuación, te presentamos las nuevas funcionalidades que te brinda esta nueva versión.
					<ul class="list-group">
						<li class="list-group-item">
							<strong>Seguimiento de casos:</strong>
							<p>
								De ahora en adelante serás capaz de brindar paso a paso un seguimiento al caso deseado hasta ser solventado, podrás ver su historial con descripciones y archivos. Si lo deseas, podrás imprimir un reporte el cual será generado hasta que el caso se encuentre solventado.
							</p>
						</li>
						<li class="list-group-item">
							<strong>Organización del comité:</strong>
							<p>
								Con esta función, llevar un historial de todos los acuerdos tomados en conjunto dentro del comité será mucho más fácil y ordenado, podrás anexar archivos como actas de reuniones entre otros, ten en cuenta que todos los archivos que agregues en esta sección serán vistos por todos los integrantes activos.
							</p>
						</li>
						<!--li class="list-group-item">
							<strong>Mensajería:</strong>
							<p>
								Podrás enviar y recibir mensajes o recordatorios con todos los integrantes del comité, así mismo recibirás peticiones de aprobación en caso de tomar una decisión en conjunto, esto para llevar un mejor historial de los casos y acuerdos dentro del comité.
							</p>
						</li-->
						<li class="list-group-item">
							<strong>Cuenta personal:</strong>
							<p>
								Hoy poseerás una cuenta individual, de esta manera las descripciones, comentarios y archivos que sean anexados a los casos o acuerdos del equipo llevarán tu firma virtual anexada a tu cuenta.
							</p>
						</li>
					</ul>
				</p>
			</div>
		</div>
		<div class="row h-3em"></div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>