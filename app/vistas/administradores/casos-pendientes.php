<?php require_once APP.'/vistas/include/header.php'; 

if ($obj->listar_casos('todo', 3))
{
	if (isset($_SESSION['clave-busqueda']))
		$datos = $obj->listar_casos('buscar', 3);
	else
		$datos = $obj->listar_casos('todo', 3);

	if (empty($datos))
	{
		$nCasos = 0;
	}
	else
	{
		$nCasos = count($datos['codigo']);
	}
}
else
{
	$nCasos = 0;
}

?>
	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-12 col-lg-8 d-col-lg">
				<h1 class="display-4"><i class="fas fa-business-time"></i> Casos Pendientes</h1>
			</div>
			<div class="col-12 col-lg-4 d-col-lg">
				<form class="form-inline mt-3" action="<?php echo URL; ?>" method="post">
					<div class="form-group mx-sm-3 mb-2">
						<?php if (isset($_SESSION['clave-busqueda'])): ?>
						<input type="text" class="form-control" name="clave" autocomplete="off" value="<?= $_SESSION['clave-busqueda'] ?>">
						<?php else: ?>
						<input type="text" class="form-control" name="clave" autocomplete="off" placeholder="Buscar...">
						<?php endif ?>
					</div>
					<button type="submit" name="buscar-caso" class="btn btn-outline-secondary mb-2">
						<i class="fas fa-search"></i> Buscar
					</button>
				</form>
			</div>
		</div>
		<div class="row mt-2">
			<div class="col-12">
				<div class="alert alert-dark">
					<table class="w-100">
						<thead>
							<tr>
								<th width="15%">Código</th>
								<th width="25%">Asunto</th>
								<th width="20%">Caso</th>
								<th width="20%">Estado</th>
								<th width="20%">Acción</th>
							</tr>
						</thead>
					</table>
				</div>
				<div id="uniqueId">
					<?php for ($i = 0; $i < $nCasos; $i++): ?>
						<div class="alert alert-light text-dark">
							<table class="w-100">
							  <tbody>
							    <tr>
							    	<th width="15%"><?php echo $datos['codigo'][$i]; ?></th>
							    	<td width="25%"><?php echo $datos['asunto'][$i]; ?></td>
							    	<td width="20%"><?php echo $datos['caso'][$i]; ?></td>
							    	<td width="20%"><?php echo $datos['estado'][$i]; ?></td>
							    	<td width="20%"><a href="<?php echo URL.'perfil-pendiente/'.$datos['codigo'][$i]; ?>">Ver Caso</a></td>
							    </tr>
							  </tbody>
							</table>
						</div>
					<?php endfor ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a class="float-right" href="<?php echo URL.'casos-antiguos'; ?>">Ver casos registrados en la versión anterior</a>
			</div>
		</div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>