<?php 

$reporte = $obj->reportes($_SESSION['caso']);

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=reporte_casos_antiguos.xls");
require_once APP."/tools/dompdf/dompdf_config.inc.php";

echo $reporte;