<?php 
require_once APP.'/vistas/include/header.php';

$usuario = $obj->info_usuario($_SESSION['codUsuario']);

if (isset($_SESSION['editar-persona']))
	$propiedad = 'required';
else
	$propiedad = 'disabled';

if (isset($_SESSION['editar-usuario']))
	$prop = 'required';
else
	$prop = 'disabled';
?>

<script>
	function Habilitar()
	{
		var txt_pass1 = document.getElementById('txt_pass1');
		var txt_pass2 = document.getElementById('txt_pass2');
		var btnSavePass = document.getElementById('btnSavePass');
		var resultado = document.getElementById('resultado');

		if (txt_pass1 != "" || txt_pass2 != "")
		{
			if (txt_pass1.value != txt_pass2.value)
			{
				btnSavePass.disabled = true;
				resultado.classList.add('text-danger');
				resultado.innerHTML = 'Las contraseñas no coinciden!!';
			}
			else
			{
				resultado.classList.remove('text-danger');
				resultado.classList.add('text-success');
				btnSavePass.disabled = false;
				resultado.innerHTML = 'Las contraseñas si coinciden!! :D';
			}
		}
		else
		{
			btnSavePass.disabled = true;
		}
	}
</script>

	<div class="container mt-5">
		<div class="row h-2em"></div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h1 class="display-4"><i class="fas fa-address-card"></i> Mi Cuenta</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 d-lg-inline-block">
				<h1 class="display-6 d-inline">Detalles de la persona</h1>
				<?php if (isset($_SESSION['editar-persona'])): ?>
				<a class="btn btn-link float-right" href="<?= URL ?>bloquear-persona">Bloquear Datos</a>
				<?php else: ?>
				<a class="btn btn-link float-right" href="<?= URL ?>editar-persona">Editar Datos</a>
				<?php endif ?>
				<hr>
			</div> 
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form action="<?= URL ?>" method="post" accept-charset="utf-8">
					<div class="form-row">
						<div class="form-group col-3">
							<label for="nombres">Nombres</label>
							<input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombre completo del usuario" autocomplete="no" value="<?= $usuario['nombres'][0] ?>" <?= $propiedad ?>>
						</div>
						<div class="form-group col-3">
							<label for="apellidos">Apellidos</label>
							<input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="alias del usuario" autocomplete="no" value="<?= $usuario['apellidos'][0] ?>" <?= $propiedad ?>>
						</div>
						<div class="form-group col-6">
							<label for="cargo">Cargo</label>
							<input type="text" class="form-control" name="cargo" id="cargo" placeholder="alias del usuario" autocomplete="no" value="<?= $usuario['cargo'][0] ?>" <?= $propiedad ?>>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-3">
							<label for="depto">Área / Sector</label>
							<input type="text" class="form-control" name="depto" id="depto" placeholder="Área / Sector" autocomplete="no" value="<?= $usuario['depto'][0] ?>" <?= $propiedad ?>>
						</div>
						<div class="form-group col-3">
							<label for="email">Correo electrónico</label>
							<input type="email" class="form-control" name="email" id="email" placeholder="yourmail@educo.org" autocomplete="no" value="<?= $usuario['email'][0] ?>" <?= $propiedad ?>>
						</div>
						<div class="form-group col-6">
							<label for="office">Oficina</label>
							<select class="form-control" name="office" id="office" <?= $propiedad ?>>
								<?php $obj->select_oficinas($usuario['codPais'][0]); ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-3">
							<label for="pais">País:</label>
							<select class="form-control" name="pais" id="pais" <?= $propiedad ?>>
								<?php $obj->select_paises(); ?>
							</select>
						</div>
						<div class="form-group col-9">
							<?php if ($usuario['estado'][0] == 'Activo'): ?>
								<p class="float-right mt-4">
									Estado del usuario: <span class="badge badge-success badge-pill mt-3">Activo</span>
								</p>
							<?php else: ?>
								<p class="float-right mt-4">
									Estado del usuario: <span class="badge badge-warning badge-pill mt-3">Inactivo</span>
								</p>
							<?php endif ?>
						</div>
					</div>
			
					<div class="form-row mt-2">
						<?php if (isset($_SESSION['editar-persona'])): ?>
						<div class="form-group col-12">
							<button type="submit" name="saveUser" id="savePerson" class="btn btn-success" value="persona"><i class="far fa-save"></i> Guardar</button>
						</div>
						<?php else: ?>
						<div class="form-group col-12">
							<a class="btn btn-success text-white" data-toggle="modal" data-target="#save">
							  	<i class="far fa-save"></i> Guardar
							</a>
						</div>
						<?php endif ?>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 d-lg-inline-block">
				<h1 class="display-6 d-inline">Detalles del usuario</h1>
				<?php if (isset($_SESSION['editar-usuario'])): ?>
				<a class="btn btn-link float-right" href="<?= URL ?>bloquear-usuario">Bloquear Datos</a>
				<?php else: ?>
				<a class="btn btn-link float-right" href="<?= URL ?>editar-usuario">Editar Datos</a>
				<?php endif ?>
				<hr>
			</div> 
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form action="<?= URL ?>" method="post" accept-charset="utf-8">
					<div class="form-row">
						<div class="form-group col-4">
							<label for="user">Usuario</label>
							<?php if (isset($_SESSION['txt_user'])): ?>
							<input type="text" class="form-control" name="user" id="user" autocomplete="no" value="<?= $_SESSION['txt_user'] ?>" <?= $prop ?>>
							<?php else: ?>
							<input type="text" class="form-control" name="user" id="user" autocomplete="no" value="<?= $usuario['usuario'][0] ?>" <?= $prop ?>>
							<?php endif ?>
						</div>
						<div class="form-group col-4">
							<label for="pass">Contraseña</label>
							<input type="password" class="form-control" name="pass" id="pass" required="true" <?= $prop ?>>
						</div>
						<div class="form-group col-4">
							<label for="pass2">Repita su contraseña</label>
							<input type="password" class="form-control" name="pass2" id="pass2" required="true" <?= $prop ?>>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12">
							<small class="text-secondary">
								Para cambiar tu usuario debes ingresar tu contraseña como confirmación.
							</small>
						</div>
					</div>
					<div class="form-row mt-2">
						<?php if (isset($_SESSION['editar-usuario'])): ?>
						<div class="form-group col-6">
							<button type="submit" name="saveUser" value="user" class="btn btn-success"><i class="far fa-save"></i> Guardar</button>
						</div>
						<?php else: ?>
						<div class="form-group col-6">
							<a class="btn btn-success text-white" data-toggle="modal" data-target="#save">
							  	<i class="far fa-save"></i> Guardar
							</a>
						</div>
						<?php endif ?>
						<div class="form-group col-6">
							<a class="btn btn-link float-right" data-toggle="modal" data-target="#changePass">
							  	<i class="fas fa-key"></i> Cambiar contraseña
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal Save -->
	<div class="modal fade" id="save" tabindex="-1" role="dialog"aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h5 class="modal-title" id="modalForget">Nota <i class="fas fa-exclamation"></i></h5>
				</div>
				<div class="modal-body">
					<p class="display-6">
						Para guardar los datos primero debes dar clic en <strong>Editar Datos</strong>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-dark" data-dismiss="modal">
						<i class="fas fa-times"></i> Cerrar
					</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal changePass -->
	<div class="modal animated fadeIn" id="changePass" tabindex="-1" role="dialog"aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-dark-green text-white">
					<h5 class="modal-title" id="modalForget"><i class="fas fa-unlock-alt"></i> Cambiar contraseña</h5>
				</div>
				<div class="modal-body">
					<form action="<?= URL ?>" method="post" accept-charset="utf-8">
						<div class="form-row">
							<div class="form-group col-9">
								<label for="txt_pass1">Ingrese su nueva contraseña</label>
								<input type="password" class="form-control" name="pass" id="txt_pass1" minlength="6" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-9">
								<label for="txt_pass2">Repita la contraseña</label>
								<input type="password" class="form-control" name="pass2" id="txt_pass2" onkeyup="Habilitar()" minlength="6" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-12">
								<button type="submit" name="saveNewPass" id="btnSavePass" class="btn btn-success">
									<i class="fas fa-key"></i> Cambiar contraseña
								</button>
								&nbsp;
								<span id="resultado"></span>
							</div>
						</div>
					</form>
					<div class="row">
						<div class="col-12">
							<p>
								Nota: Para que tu contraseña sea válida, debe tener un mínimo de 6 caracteres.
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-dark" data-dismiss="modal">
						<i class="fas fa-times"></i> Cerrar
					</button>
				</div>
			</div>
		</div>
	</div>
    
<?php require_once APP.'/vistas/include/footer.php'; ?>