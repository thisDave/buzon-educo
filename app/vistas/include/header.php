<!DOCTYPE html>
<html lang="es-SV" dir="ltr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php echo SITIO; ?></title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/animate.css">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

		<!--Icon-->
  		<link rel="shortcut icon" type="image/x-icon" href="img/icono.ico">

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="css/buzina-pagination.min.css" rel="stylesheet" type="text/css">
	</head>
	<body class="bg-white">
		<header>
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark-green">
				<a class="navbar-brand text-white" href="<?= URL ?>educo">EDUCO SV</a>
				<button class="navbar-toggler bg-dark-green" type="button" data-toggle="collapse" data-target="#contenido" aria-controls="contenido" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="contenido">

					<ul class="navbar-nav mr-auto"><?php if (isset($_SESSION['sesion'])) { $obj->menu(); } ?></ul>
					<form action="<?php echo URL; ?>" method="post" class="form-inline my-2 my-lg-0">
						<button class="btn btn-danger my-2 my-sm-0" type="submit" name="logout">
				   			<i class="fas fa-sign-out-alt"></i> Cerrar Sesión
				   		</button>
				    </form>

				</div>
			</nav>
		</header>