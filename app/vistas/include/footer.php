
		<?php
			if (isset($_SESSION['sendNow']))
			{
				if ($_SESSION['sendNow'] == 'enviar')
				{
					if($objModelo->restore_pass())
					{
						echo '
						<div class="modal fade" id="modal-success" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-dialog modal-lg" role="document">
								<div class="modal-content bg-dark-green">
									<div class="modal-header text-white">
										<h5 class="modal-title"></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true" class="text-white">&times;</span>
										</button>
									</div>
									<div class="modal-body bg-light">
										<p class="display-6">El correo ha sido enviado exitosamente, revisa tu bandeja de entrada y sigue las instrucciones.&nbsp;<img src="img/success.gif" width="35" height="35"></p>
									</div>
									<div class="modal-footer bg-light">
										<button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
									</div>
								</div>
							</div>
						</div>
						';
					}
					else
					{
						echo '
						<div class="modal fade" id="modal-fail" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-dialog modal-lg" role="document">
								<div class="modal-content bg-dark-green">
									<div class="modal-header text-white">
										<h5 class="modal-title"></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true" class="text-white">&times;</span>
										</button>
									</div>
									<div class="modal-body bg-light">
										<p class="display-6">
											<i class="fas fa-times-circle text-danger"></i>
											El correo no pudo ser enviado, por favor contacta al administrador del sitio.
										</p>
									</div>
									<div class="modal-footer bg-light">
										<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
									</div>
								</div>
							</div>
						</div>
						';
					}

					$_SESSION['sendNow'] = 'borrar';
				}
			}
		?>
		
		<?php if(isset($_COOKIE['success'])): ?>

		<div class="modal fade" id="modal-success" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-lg" role="document">
				<div class="modal-content bg-dark-green">
					<div class="modal-header text-white">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="text-white">&times;</span>
						</button>
					</div>
					<div class="modal-body bg-light">
						<p class="display-6"><?= $_COOKIE['success'] ?> &nbsp; <img src="img/success.gif" width="35" height="35"></p>
					</div>
					<div class="modal-footer bg-light">
						<button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>

		<?php if(isset($_COOKIE['fail'])): ?>

		<div class="modal fade" id="modal-fail" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog modal-lg" role="document">
				<div class="modal-content bg-dark-green">
					<div class="modal-header text-white">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="text-white">&times;</span>
						</button>
					</div>
					<div class="modal-body bg-light">
						<p class="display-6">
							<i class="fas fa-times-circle text-danger"></i>
							<?= $_COOKIE['fail'] ?>
						</p>
					</div>
					<div class="modal-footer bg-light">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>

		<?php if (isset($_COOKIE['forget'])): ?>

		<div class="modal animated bounceInDown mt-5" id="mostrarmodal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="card bg-light mb-3" style="max-width: 60rem; max-height: 100%;">
					<div class="card-header">
						<p class="display-6"><strong><?= $_COOKIE['forget'] ?></strong></p>
					</div>
					<div class="card-body">
						<div class="progress" style="height: 20px;">
						  <div class="progress-bar progress-bar-striped bg-dark-green progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php $_SESSION['sendNow'] = 'enviar'; ?>

		<meta http-equiv="refresh" content="0.5">

		<?php endif ?>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<script src="js/buzina-pagination.min.js"></script>

		<script>
			$(document).ready(function()
		    {
		    	$("#mostrarmodal").modal("show");
		    	$("#modal-success").modal("show");
		    	$("#modal-fail").modal("show");

		    	document.cookie="success=borrar; max-age= -1;";
		    	document.cookie="fail=borrar; max-age= -1;";
		    	document.cookie="forget=borrar; max-age= -1;";

		    	setTimeout(function() { $('#modal-success').modal('hide'); }, 2425);
		    });

		    $(document).ready(function()
		    {
		    	$("#newMensaje").modal("show");
		    });

		    $(document).ready(function () {
				$('#uniqueId').buzinaPagination({
					itemsOnPage:4,
					prevnext: true,
				    prevText: "Anterior",
				    nextText: "Siguiente"
				});
			});
		</script>
	</body>
</html>