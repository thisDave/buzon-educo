/*********
--------------------------------------------
| DESARROLLO PRINCIPAL DE LA BASE DE DATOS |
--------------------------------------------
Autor: David Ramos
Fecha: 02/07/2019

------------------
| MODIFICACIONES |
------------------

-------------------------------------------------------------------------------------------------------

*********/

DROP DATABASE IF EXISTS db_buzon_educo;
CREATE DATABASE IF NOT EXISTS db_buzon_educo DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE db_buzon_educo;

/**
*
* TABLA PAISES
*
**/

CREATE TABLE tbl_paises(
	codPais 	INT NOT NULL AUTO_INCREMENT,
    pais		VARCHAR(50) NOT NULL,
    CONSTRAINT 	PK_tbl_paises_codPais PRIMARY KEY (codPais)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA CASOS
*
**/

CREATE TABLE tbl_casos(
	codCaso		INT NOT NULL AUTO_INCREMENT,
    caso		VARCHAR(15) NOT NULL,
    CONSTRAINT 	PK_tbl_casos_codCaso PRIMARY KEY (codCaso)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA OFICINAS LOCALES
*
**/

CREATE TABLE tbl_oficinas_locales(
	codOl		INT NOT NULL AUTO_INCREMENT,
    oficina		VARCHAR(80) NOT NULL,
    codPais		INT NOT NULL,
    CONSTRAINT 	PK_tbl_oficinas_locales_codOl PRIMARY KEY (codOl)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA ESTADOS
*
**/

CREATE TABLE tbl_estados(
	codEstado	INT NOT NULL AUTO_INCREMENT,
    estado		VARCHAR(20) NOT NULL,
    CONSTRAINT 	PK_tbl_estados_codEstado PRIMARY KEY (codEstado)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA TIPO REGISTRO
*
**/

CREATE TABLE tbl_tipo_registros(
	codTipo			INT NOT NULL AUTO_INCREMENT,
    tipoRegistro	VARCHAR(20) NOT NULL,
    CONSTRAINT 		PK_tbl_tipo_registros_codTipo PRIMARY KEY (codTipo)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA REGISTROS
*
**/

CREATE TABLE tbl_registros(
	codRegistro		INT NOT NULL AUTO_INCREMENT,
    nombre			VARCHAR(60) NOT NULL,
    email			VARCHAR(50) NOT NULL,
    codOl			INT NOT NULL,
    asunto			VARCHAR(60) NOT NULL,
    mensaje			VARCHAR(2000) NOT NULL,
    fechaRegistro	DATE NOT NULL,
    fechaApertura	DATE,
    fechaCierre		DATE,
    codCaso			INT NOT NULL,
    codTipo			INT NOT NULL,
    codEstado		INT NOT NULL,
    codOpen			INT NOT NULL,
    codClose		INT NOT NULL,
    CONSTRAINT 		PK_tbl_registros_codRegistro PRIMARY KEY (codRegistro),
    CONSTRAINT 		FK_tbl_registros_codOl FOREIGN KEY (codOl) REFERENCES tbl_oficinas_locales (codOl),
    CONSTRAINT 		FK_tbl_registros_codCaso FOREIGN KEY (codCaso) REFERENCES tbl_casos (codCaso),
    CONSTRAINT 		FK_tbl_registros_codTipo FOREIGN KEY (codTipo) REFERENCES tbl_tipo_registros (codTipo),
    CONSTRAINT 		FK_tbl_registros_codEstado FOREIGN KEY (codEstado) REFERENCES tbl_estados (codEstado)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA REGISTROS ANTIGUOS
*
**/

CREATE TABLE tbl_registros_antiguos(
	codRegAnt		INT NOT NULL AUTO_INCREMENT,
    nombre			VARCHAR(60) NOT NULL,
    oficinaLocal	VARCHAR(75) NOT NULL,
    mensaje			VARCHAR(2000) NOT NULL,
    email			VARCHAR(50) NOT NULL,
    codCaso			INT NOT NULL,
    fechaRegistro	DATE NOT NULL,
    CONSTRAINT 		PK_tbl_registros_antiguos_codRegAnt PRIMARY KEY (codRegAnt),
    CONSTRAINT 		FK_tbl_registros_antiguos_codCaso FOREIGN KEY (codCaso) REFERENCES tbl_casos (codCaso)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA PERMISOS
*
**/

CREATE TABLE tbl_permisos(
	codPermiso	INT NOT NULL AUTO_INCREMENT,
    permiso		VARCHAR(50) NOT NULL,
    CONSTRAINT 	PK_tbl_permisos_codPermiso PRIMARY KEY (codPermiso)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA USUARIOS
*
**/

CREATE TABLE tbl_usuarios(
	codUsuario		INT NOT NULL AUTO_INCREMENT,
    nombres			VARCHAR(70) NOT NULL,
    apellidos		VARCHAR(80) NOT NULL,
    cargo			VARCHAR(60) NOT NULL,
    depto			VARCHAR(150) NOT NULL,
    usuario			VARCHAR(15) NOT NULL,
    pass			VARCHAR(500) NOT NULL,
    email			VARCHAR(60) NOT NULL,
    codOl			INT NOT NULL,
    codPermiso		INT NOT NULL,
    codEstado		INT NOT NULL,
    codPais			INT NOT NULL,
    codReset		TINYINT NOT NULL DEFAULT 0,
    primeraclave	VARCHAR(12) NOT NULL DEFAULT 'empty',
    segundaclave	VARCHAR(12) NOT NULL DEFAULT 'empty',
    CONSTRAINT		PK_tbl_usuarios_codUsuario PRIMARY KEY (codUsuario),
    CONSTRAINT		FK_tbl_usuario_codOl FOREIGN KEY (codOl) REFERENCES tbl_oficinas_locales (codOl),
    CONSTRAINT		FK_tbl_usuario_codPermiso FOREIGN KEY (codPermiso) REFERENCES tbl_permisos (codPermiso),
    CONSTRAINT		FK_tbl_usuario_codEstado FOREIGN KEY (codEstado) REFERENCES tbl_estados (codEstado),
    CONSTRAINT		FK_tbl_usuario_codPais FOREIGN KEY (codPais) REFERENCES tbl_paises (codPais)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA HISTORIAL REGISTROS
*
**/

CREATE TABLE tbl_entradas(
	codEntrada		INT NOT NULL AUTO_INCREMENT,
    titulo			VARCHAR(100) NOT NULL,
    descEntrada		VARCHAR(2000) NOT NULL,
    nombreDoc		VARCHAR(50) NOT NULL,
    rutaDoc			VARCHAR(1000) NOT NULL,
    fecha			DATE NOT NULL,
    codRegistro		INT NOT NULL,
    codUsuario		INT NOT NULL,
    CONSTRAINT		PK_tbl_entradas_codEntrada PRIMARY KEY (codEntrada),
    CONSTRAINT		FK_tbl_entradas_codRegistro FOREIGN KEY (codRegistro) REFERENCES tbl_registros (codRegistro),
    CONSTRAINT		FK_tbl_entradas_codUsuario FOREIGN KEY (codUsuario) REFERENCES tbl_usuarios (codUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA HISTORIAL ENTRADAS
*
**/

CREATE TABLE tbl_historial_entradas(
	codHistorial	INT NOT NULL AUTO_INCREMENT,
	codEntrada		INT NOT NULL,
    titulo			VARCHAR(100) NOT NULL,
    descEntrada		VARCHAR(2000) NOT NULL,
    nombreDoc		VARCHAR(50) NOT NULL,
    rutaDoc			VARCHAR(1000) NOT NULL,
    fecha			DATE NOT NULL,
    codRegistro		INT NOT NULL,
    codUsuario		INT NOT NULL,
    CONSTRAINT		PK_tbl_historial_entradas_codHistorial PRIMARY KEY (codHistorial),
    CONSTRAINT		FK_tbl_historial_entradas_codRegistro FOREIGN KEY (codRegistro) REFERENCES tbl_registros (codRegistro),
    CONSTRAINT		FK_tbl_historial_entradas_codUsuario FOREIGN KEY (codUsuario) REFERENCES tbl_usuarios (codUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA PROPUESTAS
*
**/

CREATE TABLE tbl_propuestas(
	codPropuesta	INT NOT NULL AUTO_INCREMENT,
    titulo			VARCHAR(100) NOT NULL,
    descripcion		VARCHAR(2000) NOT NULL,
    fecha			DATE NOT NULL,
    codUsuario		INT NOT NULL,
    CONSTRAINT		PK_tbl_propuestas_codPropuesta PRIMARY KEY (codPropuesta),
    CONSTRAINT		FK_tbl_propuestas_codUsuario FOREIGN KEY (codUsuario) REFERENCES tbl_usuarios (codUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/**
*
* TABLA DOCUMENTOS
*
**/

CREATE TABLE tbl_documentos(
	codDoc		INT NOT NULL AUTO_INCREMENT,
    nombreDoc	VARCHAR(50) NOT NULL,
    rutaDoc		VARCHAR(1000) NOT NULL,
    fecha		DATE NOT NULL,
    codUsuario	INT NOT NULL,
    CONSTRAINT	PK_tbl_documentos_codDoc PRIMARY KEY (codDoc),
    CONSTRAINT	FK_tbl_documentos_codUsuario FOREIGN KEY (codUsuario) REFERENCES tbl_usuarios (codUsuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
------------------------------------
| 	PROCEDIMIENTOS ALMACENADOS	   |
------------------------------------
*/

DROP PROCEDURE IF EXISTS SP_login;
DELIMITER //
CREATE PROCEDURE SP_login(
	_usuario	VARCHAR(15),
    _pass		VARCHAR(500)
)
BEGIN
	DECLARE _passEncrypted VARCHAR(500);
    SET _passEncrypted = MD5(SHA1(MD5(_pass)));
    
	IF EXISTS (SELECT * FROM tbl_usuarios WHERE usuario = _usuario AND pass = _passEncrypted) THEN
		SELECT
			u.codUsuario,
            p.permiso,
            e.estado
        FROM
			tbl_usuarios u
		JOIN
			tbl_permisos p ON u.codPermiso = p.codPermiso
		JOIN
			tbl_estados e ON u.codEstado = e.codEstado
		WHERE
			usuario = _usuario AND pass = _passEncrypted;
    END IF;
END //

DROP PROCEDURE IF EXISTS SP_infoUsuario;
DELIMITER //
CREATE PROCEDURE SP_infoUsuario( _codUsuario INT )
BEGIN
	SELECT
		u.codUsuario,
        u.nombres,
        u.apellidos,
        u.cargo,
        u.depto,
        u.usuario,
        u.email,
        o.oficina,
		p.permiso,
		e.estado,
        u.codPais,
        c.pais
	FROM
		tbl_usuarios u
	JOIN
		tbl_oficinas_locales o ON u.codOl = o.codOl
	JOIN
		tbl_permisos p ON u.codPermiso = p.codPermiso
	JOIN
		tbl_estados e ON u.codEstado = e.codEstado
	JOIN
		tbl_paises c ON u.codPais = c.codPais
	WHERE
		codUsuario = _codUsuario;
END //

DROP PROCEDURE IF EXISTS SP_registrarCaso;
DELIMITER //
CREATE PROCEDURE SP_registrarCaso(
	_codTipo	INT,
    _nombre		VARCHAR(60),
    _email		VARCHAR(50),
    _codOl		INT,
    _codCaso	INT,
    _asunto		VARCHAR(60),
    _mensaje	VARCHAR(2000),
    _fecha 		DATE
)
BEGIN
	INSERT INTO tbl_registros VALUES
    (NULL, _nombre, _email, _codOl, _asunto, _mensaje, _fecha, null, null, _codCaso, _codTipo, 3, 0, 0);
END //

DROP PROCEDURE SP_listarCasos;
DELIMITER //
CREATE PROCEDURE SP_listarCasos( _condicion VARCHAR(50), _cond INT)
BEGIN

SET @consulta = CONCAT('
SELECT
	CASE WHEN
		t.tipoRegistro = \'Personal\'
	THEN
		concat(\'RP00\',r.codRegistro)
	ELSE
		concat(\'RA00\',r.codRegistro)
	END
		AS \'codigo\',
	r.nombre,
	r.email,
	o.oficina,
	r.asunto,
	r.mensaje,
	DATE_FORMAT(r.fechaRegistro, "%d-%m-%Y") AS \'fechaRegistro\',
    DATE_FORMAT(r.fechaApertura, "%d-%m-%Y") AS \'fechaApertura\',
    DATE_FORMAT(r.fechaCierre, "%d-%m-%Y") AS \'fechaCierre\',
	c.caso,
	t.tipoRegistro,
	e.estado,
    r.codOpen,
    r.codClose
FROM
	tbl_registros r
JOIN
	tbl_oficinas_locales o ON r.codOl = o.codOl
JOIN
	tbl_casos c ON r.codCaso = c.codCaso
JOIN
	tbl_tipo_registros t ON r.codTipo = t.codTipo
JOIN
	tbl_estados e ON r.codEstado = e.codEstado ', _condicion);

PREPARE stmt FROM @consulta;

SET @cond = _cond;

EXECUTE stmt USING @cond;

DEALLOCATE PREPARE stmt;

END //

DROP PROCEDURE IF EXISTS SP_buscar;
DELIMITER //
CREATE PROCEDURE SP_buscar( _clave VARCHAR(50), _codigo INT )
BEGIN
	SELECT
		CASE WHEN
			t.tipoRegistro = 'Personal'
		THEN
			concat('RP00',r.codRegistro)
		ELSE
			concat('RA00',r.codRegistro)
		END
			AS 'codigo',
		r.nombre,
		r.email,
		o.oficina,
		r.asunto,
		r.mensaje,
		DATE_FORMAT(r.fechaRegistro, "%d-%m-%Y") AS 'fechaRegistro',
		DATE_FORMAT(r.fechaApertura, "%d-%m-%Y") AS 'fechaApertura',
		DATE_FORMAT(r.fechaCierre, "%d-%m-%Y") AS 'fechaCierre',
		c.caso,
		t.tipoRegistro,
		e.estado,
		r.codOpen,
		r.codClose
	FROM
		tbl_registros r
	JOIN
		tbl_oficinas_locales o ON r.codOl = o.codOl
	JOIN
		tbl_casos c ON r.codCaso = c.codCaso
	JOIN
		tbl_tipo_registros t ON r.codTipo = t.codTipo
	JOIN
		tbl_estados e ON r.codEstado = e.codEstado
	WHERE
		r.codEstado = _codigo
        AND r.asunto LIKE CONCAT('%',_clave,'%');

END //

DROP PROCEDURE IF EXISTS SP_listarCasosAntiguos;
DELIMITER //
CREATE PROCEDURE SP_listarCasosAntiguos()
BEGIN
	SELECT
		r.codRegAnt AS 'codigo',
        r.nombre,
        r.email,
        r.oficinaLocal AS 'oficina',
        DATE_FORMAT(r.fechaRegistro, "%d-%m-%Y") AS 'fechaRegistro',
        c.caso,
        r.mensaje
	FROM
		tbl_registros_antiguos r
	JOIN
		tbl_casos c ON r.codCaso = c.codCaso
	ORDER BY
		r.codRegAnt;
END //

DROP PROCEDURE IF EXISTS SP_abrirCaso;
DELIMITER //
CREATE PROCEDURE SP_abrirCaso( _cod INT, _codUser INT, _fecha DATE )
BEGIN
	UPDATE tbl_registros SET codEstado = 4, codOpen = _codUser, fechaApertura = _fecha WHERE codRegistro = _cod;
END //

DROP PROCEDURE IF EXISTS SP_ingresarEntrada;
DELIMITER //
CREATE PROCEDURE SP_ingresarEntrada(
	_titulo			VARCHAR(100),
    _descripcion	VARCHAR(2000),
    _codRegistro	INT,
    _codUser		INT,
    _fecha			DATE
)
BEGIN
	INSERT INTO tbl_entradas VALUES (NULL, _titulo, _descripcion, '', '', _fecha, _codRegistro, _codUser);
END//

DROP PROCEDURE IF EXISTS SP_ingresarDocumento;
DELIMITER //
CREATE PROCEDURE SP_ingresarDocumento(
	_codEntrada	INT,
    _nombreDoc	VARCHAR(50),
    _rutaDoc	VARCHAR(1000),
    _codUsuario	INT
)
BEGIN
	UPDATE tbl_entradas SET nombreDoc = _nombreDoc, rutaDoc = _rutaDoc, codUsuario = _codUsuario WHERE codEntrada = _codEntrada;
END //

DROP PROCEDURE IF EXISTS SP_newDoc;
DELIMITER //
CREATE PROCEDURE SP_newDoc(
    _nombreDoc	VARCHAR(50),
    _rutaDoc	VARCHAR(1000),
    _codUsuario	INT
)
BEGIN
	INSERT INTO tbl_documentos VALUES (NULL, _nombreDoc, _rutaDoc, CURDATE(), _codUsuario);
END //

DROP PROCEDURE IF EXISTS SP_listarEntradas;
DELIMITER //
CREATE PROCEDURE SP_listarEntradas( _cod INT )
BEGIN
	SELECT
		e.codEntrada,
        e.titulo,
        e.descEntrada,
        e.nombreDoc,
        e.rutaDoc,
        DATE_FORMAT(e.fecha, "%d-%m-%Y") AS 'fecha',
        CONCAT(u.nombres, ' ', u.apellidos) AS 'responsable'
	FROM
        tbl_entradas e
	JOIN
		tbl_usuarios u ON e.codUsuario = u.codUsuario
	WHERE
		e.codRegistro = _cod;
END //

DROP PROCEDURE IF EXISTS SP_verDocumento;
DELIMITER //
CREATE PROCEDURE SP_verDocumento( _cod INT, _tipo INT )
BEGIN
	IF _tipo = 1 THEN
		SELECT rutaDoc FROM tbl_entradas WHERE codEntrada = _cod;
    ELSE
		SELECT rutaDoc FROM tbl_documentos WHERE codDoc = _cod;
    END IF;
END //

DROP PROCEDURE IF EXISTS SP_editarEntrada;
DELIMITER //
CREATE PROCEDURE SP_editarEntrada(
	_titulo 		VARCHAR(100),
    _descEntrada	VARCHAR(2000),
    _codEntrada		INT,
    _codUsuario		INT
)
BEGIN
	UPDATE tbl_entradas SET titulo = _titulo, descEntrada = _descEntrada, codUsuario = _codUsuario WHERE codEntrada = _codEntrada;
END //

DROP PROCEDURE IF EXISTS SP_eliminarDocumento;
DELIMITER //
CREATE PROCEDURE SP_eliminarDocumento(
    _codEntrada		INT,
    _codUsuario		INT
)
BEGIN
	UPDATE tbl_entradas SET nombreDoc = '', rutaDoc = '' WHERE codEntrada = _codEntrada;
END //

DROP PROCEDURE IF EXISTS SP_eliminarEntrada;
DELIMITER //
CREATE PROCEDURE SP_eliminarEntrada( _codEntrada INT, _codRegistro INT, _codUsuario INT, _fecha DATE )
BEGIN
	INSERT INTO
		tbl_historial_entradas
	VALUES
		(NULL, _codEntrada, 'ELIMINADO', 'ELIMINADO', 'ELIMINADO', 'ELIMINADO', _fecha, _codRegistro, _codUsuario);

    DELETE FROM tbl_entradas WHERE codEntrada = _codEntrada;

END //

DROP PROCEDURE IF EXISTS SP_cerrarCaso;
DELIMITER //
CREATE PROCEDURE SP_cerrarCaso(
	_codRegistro 	INT,
    _codUsuario		INT,
    _fecha			DATE
)
BEGIN
	UPDATE tbl_registros SET codEstado = 5, codClose = _codUsuario, fechaCierre = _fecha WHERE codRegistro = _codRegistro;
END //

DROP PROCEDURE IF EXISTS SP_charts;
DELIMITER //
CREATE PROCEDURE SP_charts(_fecha DATE)
BEGIN
	SELECT
		r.codRegistro,
        t.tipoRegistro,
        c.caso,
        r.codEstado,
        r.fecharegistro,
        MONTH(r.fechaRegistro) AS 'mes',
        YEAR(r.fechaRegistro) AS 'año'
	FROM
		tbl_registros r
	JOIN
		tbl_tipo_registros t ON r.codTipo = t.codTipo
	JOIN
		tbl_casos c ON r.codCaso = c.codCaso
	WHERE
		YEAR(r.fechaRegistro) = YEAR(_fecha);
END //

DROP PROCEDURE IF EXISTS SP_historial_entrada;
DELIMITER //
CREATE PROCEDURE SP_historial_entrada( _codEntrada INT )
BEGIN
	SELECT
		h.codHistorial,
		h.titulo,
        h.descEntrada,
        h.nombreDoc,
        h.rutaDoc,
        h.fecha,
        CONCAT(u.nombres, ' ', u.apellidos) AS 'nombre'
	FROM
		tbl_historial_entradas h
	JOIN
		tbl_usuarios u ON h.codUsuario = u.codUsuario
	WHERE
		codEntrada = _codEntrada;
END //

DROP PROCEDURE IF EXISTS SP_nueva_propuesta;
DELIMITER //
CREATE PROCEDURE SP_nueva_propuesta( _titulo VARCHAR(100), _descripcion VARCHAR(2000), _codUsuario INT, _fecha DATE)
BEGIN
	INSERT INTO
		tbl_propuestas
    VALUES
		(NULL, _titulo, _descripcion, _fecha, _codUsuario);
END //

DROP PROCEDURE IF EXISTS SP_update_pass;
DELIMITER //
CREATE PROCEDURE SP_update_pass( _pass VARCHAR(100), _codUsuario INT)
BEGIN
	UPDATE tbl_usuarios SET pass = MD5(SHA1(MD5(_pass))), codReset = 0 WHERE codUsuario = _codUsuario;
END //

/*
------------------------------------
|              TRIGGERS 		   |
------------------------------------
*/


DROP TRIGGER IF EXISTS InsertarHistorial_insert;
DELIMITER //
CREATE TRIGGER InsertarHistorial_insert AFTER INSERT
ON	tbl_entradas
FOR EACH ROW
BEGIN
	INSERT INTO
		tbl_historial_entradas (codEntrada, titulo, descEntrada, nombreDoc, rutaDoc, fecha, codRegistro, codUsuario)
	VALUES
		(NEW.codEntrada, NEW.titulo, NEW.descEntrada, NEW.nombreDoc, NEW.rutaDoc, NEW.fecha, NEW.codRegistro, NEW.codUsuario);
END //

DROP TRIGGER IF EXISTS InsertarHistorial_update;
DELIMITER //
CREATE TRIGGER InsertarHistorial_update AFTER UPDATE
ON	tbl_entradas
FOR EACH ROW
BEGIN
	INSERT INTO
		tbl_historial_entradas (codEntrada, titulo, descEntrada, nombreDoc, rutaDoc, fecha, codRegistro, codUsuario)
	VALUES
		(NEW.codEntrada, NEW.titulo, NEW.descEntrada, NEW.nombreDoc, NEW.rutaDoc, NEW.fecha, NEW.codRegistro, NEW.codUsuario);
END //

/*
------------------------------------
|         INSERCIÓN DE DATOS 	   |
------------------------------------
*/

INSERT INTO tbl_paises (codPais, pais) VALUES
(NULL, 'El Salvador'),
(NULL, 'Guatemala'),
(NULL, 'Nicaragua'),
(NULL, 'Bolivia');

INSERT INTO tbl_casos (codCaso, caso) VALUES
(NULL, 'Queja'),
(NULL, 'Sugerencia'),
(NULL, 'Felicitaciones');

INSERT INTO tbl_oficinas_locales (codOl, oficina, codPais) VALUES
(NULL, 'Región La Libertad', 1),
(NULL, 'Región San Salvador', 1),
(NULL, 'Región San Vicente', 1),
(NULL, 'Región San Miguel', 1),
(NULL, 'Región Guatemala', 2),
(NULL, 'Región Nicaragua', 3),
(NULL, 'Región Bolivia', 4);

INSERT INTO tbl_estados (codEstado, estado) VALUES
(NULL, 'Activo'),
(NULL, 'Inactivo'),
(NULL, 'Pendiente'),
(NULL, 'En proceso'),
(NULL, 'Cerrado');

INSERT INTO tbl_tipo_registros (codTipo, tipoRegistro) VALUES
(NULL, 'Personal'),
(NULL, 'Anonimo');

/* REGISTROS DE PRUEBA */

/*
INSERT INTO tbl_registros (codRegistro, nombre, email, codOl, asunto, mensaje, fechaRegistro, codCaso, codTipo, codEstado, codOpen, codClose) VALUES
(1, 'David Ramos', 'isaacdrq@gmail.com', 1, 'Confidencialidad de procesos', 'Se sugiere mantener mejor confidencialidad al atender un caso del buzon.', '2019-01-01', 2, 1, 3, 0, 0),
(2, 'N/A', 'N/A', 1, 'Bullyng', 'Se informa sobre un caso de bullyng en la oficina local por parte del compañero', '2019-01-01', 1, 2, 3, 0, 0),
(3, 'David Ramos', 'isaacdrq@gmail.com', 1, 'Felicitaciones', 'Se felicita al equipo de quejas y sugerencias por su pronta acción ante los problemas informados', '2019-01-01', 3, 1, 3, 0, 0);
*/

INSERT INTO tbl_registros_antiguos (codRegAnt, nombre, oficinaLocal, mensaje, email, codCaso, fechaRegistro) VALUES
(1, 'Anonimo', 'Central, San Salvador', 'Rosa Virginia Sanchez Cuellar ', 'N/A', 2, '2016-10-18'),
(2, 'Anonimo', 'Central, San Salvador', 'Seria bueno incluir al personal administrativo de TERRAS a jornadas de enfoques de derecho y que conozcas la política de buen trato.', 'N/A', 2, '2016-10-18'),
(3, 'Guillermo Jesus Flores Pocasangre', 'Olocuilta, La Paz', 'Integrante del comité sugiero a don Israel', 'guillermo.flores@educo.org', 2, '2016-10-18'),
(4, 'Anonimo', 'Central, San Salvador', 'Comentario: el sistema de votación para el comité de quejas y sugerencias no garantiza que quede un representante por cada centro de trabajo y considero conveniente que al menos una persona sea de cada TERRA ya que conocen el contexto de su centro.', 'N/A', 2, '2016-10-18'),
(5, 'Claudia Guadalupe Navas Aguilar', 'Olocuilta, La Paz', 'No hay en este momento sugerencias o queja alguna', 'claudia.navas@educo.org', 2, '2016-10-18'),
(6, 'Carlos Borromeo Samayoa', 'Oscicala, Morazán', 'No tengo sugerencias ni quejas, solo que en el correo se me indica que participe en la eleccion del integrante del Comite de quejas y al ingresar al Link, me solicitan quejas o sugerencias...no entiendo su relacion ......saludos compañeros/as', 'carlos.samayoa@educo.org', 2, '2016-10-18'),
(7, 'Rolando Mauricio Cruz', 'Central, San Salvador', 'no vote no me dio la opción ', 'rolando.cruz@educo.org', 1, '2016-10-19'),
(8, 'Anonimo', 'Olocuilta, La Paz', 'No hay sugerencias', 'N/A', 2, '2016-10-26'),
(9, 'Isaac David Ramos Quintanilla', 'Central, San Salvador', 'sugerencia de prueba...', 'isaac.ramos@educo.org', 2, '2016-11-28'),
(10, 'Isaac David Ramos Quintanilla', 'Central, San Salvador', 'Mi sugerencia', 'isaac.ramos@educo.org', 2, '2016-11-28'),
(11, 'Manuel Alfredo Gomez Cuenca', 'Central, San Salvador', 'Ej. Espero que la alimentación sea cancelada a tiempo (esta es una prueba del mecanismo de correo - validación)', 'manuel.gomez@educo.org', 2, '2016-11-28'),
(12, 'Anonimo', 'Central, San Salvador', 'Ej. Esta es una prueba de queja para verificar el funcionamiento del mecanismo (Manuel Gómez)', 'N/A', 1, '2016-11-28'),
(13, 'Anonimo', 'Central, San Salvador', 'mi queja', 'N/A', 1, '2017-01-20'),
(14, 'Isaac David Ramos Quintanilla', 'Central, San Salvador', 'Esta es una sugerencia de prueba...', 'isaac.ramos@educo.org', 2, '2017-03-09'),
(15, 'Anonimo', 'Central, San Salvador', 'Esta es una queja de prueba', 'N/A', 1, '2017-03-09'),
(16, 'Anonimo', 'Central, San Salvador', 'Sugerencia de prueba.....', 'N/A', 2, '2017-03-10'),
(17, 'Erick Dexahi Romero Mena', 'Jayaque, La Libertad', 'Sugiero trabajar la estrategia de socialización y sensibilización del comité, elaborar un flujo grama que visualice como esta estrucuturado y las funciones y roles que desempeñan.\r\n\r\n', 'erick.romero@educo.org', 2, '2017-03-31'),
(18, 'Orsy Godofredo Romero Valladares', 'San Vicente, San Vicente', 'Buenas tardes, gracias por este espacio para compartir nuestras quejas y sugerencias, remito a ustedes que hemos pasado un trimestre y no se nos ha hecho llegar las chumpas rompe vientos andamos varios compañeros en motocicletas y la ropa que andamos como chumpas ya no sirven y tienen logos viejos.  estas son importantes para nuestro trabajo ya que andamos en motocicletas y exponemos nuestra vida todos los días. ', 'orsy.romero@educo.org', 1, '2017-05-03'),
(19, 'Zulma Maritza Aguilar Aguilar', 'Olocuilta, La Paz', 'Primero darle gracias a la fundación por este espacio; mi sugerencia es respecto al seguro médico privado de cada uno de nosotros, yo hago uso de esta prestación por emergencias,  solicitar que esta prestación se mantenga como el año pasado,  el pago de los $ 75.00 como deducible es un costo adicional que sumado al pago del 100% de los costos de  medicamento afectara la  economía familiar del personal que hacemos uso de la prestación y no podremos accesar a los medicamentos por no contar con el dinero para la compra; por experiencia el tiempo de espera  para el reintegro de los gastos es muy tardado ( hasta 5 meses)  por ello sugiero que se mantenga la prestación del seguro medico tal como el año pasado; sin el costo del deducible para medicamentos y pagando el 20% correspondiente al coaseguro.\r\n\r\nen espera de su respuesta.', 'roxana.garcia@educo.org', 2, '2017-05-05'),
(20, 'Anonimo', 'Jayaque, La Libertad', 'Mi sugerencia esta encaminada al buen uso de los vehículos institucionales, en la oficina de La Libertad parece que al nuevo administrador le han asignado un vehículo a su disposición, algo que no se ve bien teniendo en cuenta los recorridos que se tienen que hacer en esta zona, por lo que seria bueno que se priorice la movilidad de un colectivo mas amplio y no para una persona. ', 'N/A', 2, '2017-05-11'),
(21, 'Manuel Alfredo Gomez Cuenca', 'Central, San Salvador', 'Queja de prueba en encuentro regional...', 'manuel.gomez@educo.org', 1, '2017-08-29'),
(22, 'Anonimo', 'Jayaque, La Libertad', 'Quiero expresar mi descontento con Sergio Padilla, ya que este señor se esta aprovechando de manera descarada de su compañero Jerry. Sergio se dedica felizmente a sus negocios mientras Jerry hace todo su trabajo. Sergio es una persona conflictiva quien fista mucho de ser un empleado que este bajo los nuevos intereses institucionales. No entiendo como es posible que sigan manteniendo a una persona que no trabaja y cobra su salario ademas es de los que se queja de las prestaciones que gratamente tenemos. Espero puedan hacer algo. Por salud mental del compañero Jerry.', 'N/A', 1, '2017-09-13'),
(23, 'Anonimo', 'Jayaque, La Libertad', 'Quiero hacer de manera efectiva mi queja en relación a Sergio Padilla, es una persona que esta lucrándose de manera descara de la institución, no hace su trabajo de manera efectiva y deja que Jerry incurra en toda la carga de trabajo, es ofensivo y despectivo, tratando a sus compañeros con sobre nombres como ENANO y Abuelo de lo sipmson a dos compañeros mayores de edad, si la institucional quiere mejorar sus estándares debería de pensar mejor en contar o no con personas como el.', 'N/A', 1, '2017-09-18'),
(24, 'Anonimo', 'Jayaque, La Libertad', 'Reflexionando sobre el trabajo que se viene y los estandares de participacion, quiero exponer sobre el comportamiento de una compañera, Sadra Rios.\r\nEs una persona que frente a las personas en central muestra una cara amable colaboradora y muy pensante, pero en la oficina es otra cosa, a Sandra le gusta entrometerse en la vida de los demás y anda averiguando y levantando calumnias de manera mal intencionada y discriminada. Ella se atiene que en central tiene relación con personas importantes como ella le llama. No es coherente con el trabajo y seria interesante que pudieran valorar el trabajo que hace en campo, es una persona que tiene un vocabulario muy soez y despectivo, se refiere a los y niños y jóvenes como \"pendejos\" o \"cerotes\" esto lo dice con las personas cercanas, no lo dice en el centro escolar, es importante sanear las oficina de este tipo de personas venenosas que no aportan al clima organizacional, entiendo los años que ella tiene de trabajar para la institución y quiza s', 'N/A', 1, '2017-09-21'),
(25, 'Anonimo', 'Jayaque, La Libertad', 'Tengo una consulta, porque solo van a evaluar a los de apadrinamiento para contratar el próximo año, en el proyecto hacia una escuela saludable y segura hay técnicos y asistentes que no merecen estar ahí, evalúenlos no solo los tengas por caras bonitas, ese es el cambio que quieren hacer? O es más de lo mismo tener cuello para seguir en educo? ', 'N/A', 1, '2017-10-28'),
(26, 'Jhonattan Kervin Rodriguez Aragon', 'Jayaque, La Libertad', 'Test.\r\nMejorar el  café ', 'kervin.rodriguez@educo.org', 2, '2018-05-30'),
(27, 'Vanessa Carmen Valladares Martinez', 'San Vicente, San Vicente', 'No tiene el apartado de reconocimiento la plataforma', 'alma.regalado@educo.org', 1, '2018-07-20'),
(28, 'Anonimo', 'Central, San Salvador', 'Reconocimiento de prueba.', 'N/A', 3, '2018-08-31'),
(29, 'Juan Alfonso Moreno', 'San Vicente, San Vicente', 'En una visita realizada a la oficina local de Jayaque, al entran al baño visualice que el recipiente donde se tiran los papeles , es con una tapadera que gira, lo cual lo vuelve un foco d infección, dado a que en la tapadera quedan las bacterias del papel utilizado.\r\nSe sugiere hacer una visita y verificar lo antes dicho, si ya se cambio el recipiente por uno del que la tapadera se levanta haciendo la acción con el pie, hacer caso omiso; y si no se ha realizado hacer el cambio oportuno.', 'jamoreno@educo.org', 2, '2018-09-05');

INSERT INTO tbl_permisos (codPermiso, permiso) VALUES
(NULL, 'administrador'),
(NULL, 'usuarios'),
(NULL, 'integrante');

INSERT INTO tbl_usuarios (codUsuario, nombres, apellidos, cargo, depto, usuario, pass, email, codOl, codPermiso, codEstado, codPais) VALUES
(NULL, 'Administrador', 'Educo', 'Desarrollador', 'Informática', 'admin', MD5(SHA1(MD5('Buzon@2021'))), 'isaac.ramos@educo.sv', 2, 1, 1, 1),
(NULL, 'David', 'Ramos', 'Desarrollador', 'Informática', 'isaac.ramos', MD5(SHA1(MD5('Educo2021'))), 'isaac.ramos@educo.org', 2, 1, 1, 1),
(NULL, 'William Fabricio', 'Vargas Menjivar', 'Técnico en Gestión de talento humano', 'Gestión de talento humano', 'fabricio.vargas', MD5(SHA1(MD5('fabricio.vargas'))), 'fabricio.vargas@educo.org', 2, 3, 1, 1);

/*
------------------------------------
|  		      TESTEOS			   |
------------------------------------
*/

